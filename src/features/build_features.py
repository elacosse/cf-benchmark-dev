# -*- coding: utf-8 -*-
import click
import logging
from pathlib import Path
import pandas as pd
from dotenv import find_dotenv, load_dotenv
import os
import sys
import numpy as np
from nilearn.decomposition import CanICA, DictLearning
from pca import fast_svd, reduce_single
from feature_utils import compute_parpca_roi_reg, compute_semidense_connectome, glm, dual_regression_glm, load_contrasts
from sklearn.random_projection import GaussianRandomProjection

@click.command()
@click.argument('yml_experiment_name')
def main(yml_experiment_name):

    """ Runs data processing scripts to turn processed data from (../processed) into
        cleaned data ready to be analyzed (saved in ../processed/features).

        Assumes processed data in ../../data/{type_id}/{subject_id}.npy
    """
    logger = logging.getLogger(__name__)
    logger.info('running experiment <-- ' + yml_experiment_name)
    logger.info('path to yml: ' + os.path.join(project_dir, 'src', 'experiments'))
    load_dotenv(os.path.join(project_dir, 'src', 'experiments', yml_experiment_name))
    logger.info('making features for models ')

    HCP_BASEDIR = os.getenv('HCP_BASEDIR')
    RANDOM_SEED = int(os.getenv('RANDOM_SEED'))
    SUBJECT_CSV_NAME = os.getenv('SUBJECT_CSV_NAME')
    CONTRAST_IDS = [int(c) for c in os.getenv('CONTRAST_INDICES')[1:-1].split(',')]
    logger.info('Random state for process: ' + str(RANDOM_SEED))

    # get subject ids
    subjects_df = pd.read_csv(os.path.join(project_dir, 'data/external/subject_lists/', SUBJECT_CSV_NAME))
    subjects = subjects_df['Subject']
    subjects = subjects

    DATA_TYPE=os.getenv('DATA_TYPE')
    DO_PCA=int(os.getenv('DO_FPCA'))
    DO_SPPCA=int(os.getenv('DO_FSPPCA'))
    DO_GICA=int(os.getenv('DO_FGICA'))
    DO_TASK_GICA=int(os.getenv('DO_FTASK_GICA'))
    DO_MMP=int(os.getenv('DO_FMMP'))
    DO_GRP=int(os.getenv('DO_FGRP'))
    DO_AFMOD=int(os.getenv('DO_FAFMOD'))
    
    if not os.path.exists(os.path.join(project_dir, 'data/processed/features', DATA_TYPE)):
        os.makedirs(os.path.join(project_dir, 'data/processed/features', DATA_TYPE))
    ######################################################
    # Create PCA components and save to processed
    if DO_PCA:
        data_load_temp = os.path.join(project_dir,'data/processed/{type_id}/{subject_id}_rest_data.npy') 
        N_COMPONENTS = int(os.getenv('PCA_COMPONENTS'))
        pca_data_save_temp = os.path.join(project_dir,'data/processed/features/{type_id}/pca/{subject_id}.npy')
        if not os.path.exists(os.path.join(project_dir,'data/processed/features', DATA_TYPE, 'pca')):
            os.makedirs(os.path.join(project_dir,'data/processed/features', DATA_TYPE, 'pca'))
        # do single PCA over subjects if not there
        for subject_id in subjects:
            logger.info('rsfmri pca on subject ' + str(subject_id))
            if not os.path.isfile(pca_data_save_temp.format(type_id=DATA_TYPE, subject_id=subject_id)):
                img_data = np.load(data_load_temp.format(type_id=DATA_TYPE, subject_id=subject_id))
                X = reduce_single(img_data, confound=None, n_samples=N_COMPONENTS,random_state=RANDOM_SEED)
                np.save(pca_data_save_temp.format(type_id=DATA_TYPE, subject_id=subject_id), X)

        # if task data available
        data_load_temp = os.path.join(project_dir,'data/processed/{type_id}/{subject_id}_task_data.npy') 
        try:
            if not os.path.exists(os.path.join(project_dir,'data/processed/features', 'tfmri', 'pca')):
                os.makedirs(os.path.join(project_dir,'data/processed/features', 'tfmri', 'pca'))
            # do single PCA over subjects if not there
            for subject_id in subjects:
                logger.info('task pca on subject ' + str(subject_id))
                if not os.path.isfile(pca_data_save_temp.format(type_id='tfmri', subject_id=subject_id)):
                    img_data = np.load(data_load_temp.format(type_id='tfmri', subject_id=subject_id))
                    X = reduce_single(img_data, confound=None, n_samples=N_COMPONENTS,random_state=RANDOM_SEED)
                    np.save(pca_data_save_temp.format(type_id='tfmri', subject_id=subject_id), X)
        except Exception as e:
            logger.info("task data not found")
    ######################################################
    # Extract SPPCA features
    # REQUIRES SPATIAL INFORMATION IN /data/interim/distance
    if DO_SPPCA:
        data_load_temp = os.path.join(project_dir,'data/processed/{type_id}/{subject_id}_rest_data.npy') 
        exclusion_dist = int(os.getenv('PCR_EXCLUSION_DIST')) # mm
        ncomponents=int(os.getenv('PCR_COMPONENTS'))
        whiten=False
        save_temp = os.path.join(project_dir,'data/processed/features/{type_id}/sppcr/{subject_id}.npy')
        if not os.path.exists(os.path.join(project_dir,'data/processed/features/{type_id}/sppcr').format(type_id=DATA_TYPE)):
            os.makedirs(os.path.join(project_dir,'data/processed/features/{type_id}/sppcr').format(type_id=DATA_TYPE))

        A = np.load(os.path.join(project_dir, 'data/external/hcp_parc_379.npy'))
        # load spatial mask information
        distance = np.load(os.path.join(project_dir, 'data/interim/distance/', 'cortical_distance.npy'))
        sub_distance = np.load(os.path.join(project_dir, 'data/interim/distance/', 'subcortical_distance.npy'))

        for subject_id in subjects:
            feat_mat = np.zeros((379, 91282))
            img_data = np.load(data_load_temp.format(type_id=DATA_TYPE, subject_id=subject_id))
            img_data = img_data.T
            for roi_id in range(0, 379):
                roi_sink_mask = A[roi_id] > 0
                #print(roi_id, np.sum(roi_sink_mask))
                if roi_id < 360:
                    roi_dist = distance[A[roi_id,0:59412] == 1]
                    roi_dist = (roi_dist <= exclusion_dist) & (roi_dist > 0)
                    roi_source_mask = ~(np.sum(roi_dist, axis=0) > 0)
                    roi_source_mask = np.concatenate([roi_source_mask, np.ones(31870,dtype=bool)]) # dim (31870) of subcortical volume 
                    feat_array, par_interaction_mat = compute_parpca_roi_reg(img_data, roi_source_mask, roi_sink_mask, ncomponents=ncomponents, random_state=RANDOM_SEED, whiten=whiten)
                    feat_mat[roi_id, roi_source_mask] = feat_array
                if roi_id >= 360:
                    roi_dist = sub_distance[A[roi_id,59412:] == 1]
                    roi_dist = (roi_dist <= exclusion_dist) & (roi_dist > 0)
                    roi_source_mask = ~(np.sum(roi_dist, axis=0) > 0)
                    roi_source_mask = np.concatenate([np.ones(59412,dtype=bool), roi_source_mask])
                    feat_array, par_interaction_mat = compute_parpca_roi_reg(img_data, roi_source_mask, roi_sink_mask, ncomponents=ncomponents, random_state=RANDOM_SEED, whiten=whiten)
                    feat_mat[roi_id, roi_source_mask] = feat_array
            np.save(save_temp.format(type_id=DATA_TYPE, subject_id=subject_id), feat_mat)
    ######################################################
    # Create Group ICA components
    # Must run PCA routines first
    if DO_GICA:
        data_load_temp = os.path.join(project_dir,'data/processed/{type_id}/{subject_id}_rest_data.npy') 
        N_COMPONENTS = int(os.getenv('PCA_COMPONENTS'))
        pca_data_load_temp = os.path.join(project_dir,'data/processed/features/{type_id}/pca/{subject_id}.npy')
        save_temp = os.path.join(project_dir,'data/processed/features/{type_id}/ica/{variant_id}_{subject_id}.npy')

        pca_data = []
        if not os.path.exists(os.path.join(project_dir,'data/processed/features/{type_id}/ica'.format(type_id=DATA_TYPE))):
            os.makedirs(os.path.join(project_dir, 'data/processed/features', DATA_TYPE, 'ica'))
        for subject_id in subjects:
            X = np.load(pca_data_load_temp.format(type_id=DATA_TYPE, subject_id=subject_id))
        pca_data.append(X[0:N_COMPONENTS,:])
        X = np.concatenate(pca_data, axis=0)
        del pca_data

        # # CANICA
        canica = CanICA(n_components=N_COMPONENTS, smoothing_fwhm=None,
                    memory=None, memory_level=2, do_cca=True,
                    threshold=None, verbose=10, random_state=RANDOM_SEED, n_jobs=-2)
        canica._raw_fit(X)
        A = canica.components_

        # Generate features 
        pinvA = np.linalg.pinv(A)
        for subject_id in subjects:
            if not os.path.isfile(save_temp.format(type_id=DATA_TYPE, variant_id='standard', subject_id=subject_id)):
                logger.info('ica sd-connectome on subject ' + str(subject_id))
                img_data = np.load(data_load_temp.format(type_id=DATA_TYPE, subject_id=subject_id))
                variant_id = 'standard'
                F = compute_semidense_connectome(img_data, A, normalize=True)
                np.save(save_temp.format(type_id=DATA_TYPE, variant_id=variant_id, subject_id=subject_id), F)
                # variant_id = 'dualreg'
                # t, cope, varcope = dual_regression_glm(img_data, pinvA=pinvA)
                # F = compute_semidense_connectome(img_data, t, normalize=True)
                # np.save(save_temp.format(type_id=DATA_TYPE, variant_id=variant_id, subject_id=subject_id), F)
    ######################################################
    # Create Group ICA components from task components
    if DO_TASK_GICA:
        data_load_temp = os.path.join(project_dir,'data/processed/{type_id}/{subject_id}_rest_data.npy') 
        N_COMPONENTS = int(os.getenv('PCA_COMPONENTS'))
        pca_data_load_temp = os.path.join(project_dir,'data/processed/features/tfmri/pca/{subject_id}.npy')
        save_temp = os.path.join(project_dir,'data/processed/features/{type_id}/ica/{variant_id}_{subject_id}.npy')

        pca_data = []
        if not os.path.exists(os.path.join(project_dir,'data/processed/features/{type_id}'.format(type_id='tfmri'))):
            os.makedirs(os.path.join(project_dir, 'data/processed/features', DATA_TYPE, 'ica-rest-task'))
        for subject_id in subjects:
            X = np.load(pca_data_load_temp.format(type_id='tfmri', subject_id=subject_id))
        pca_data.append(X[0:N_COMPONENTS,:])
        X = np.concatenate(pca_data, axis=0)
        del pca_data

        # # CANICA
        canica = CanICA(n_components=N_COMPONENTS, smoothing_fwhm=None,
                    memory=None, memory_level=2, do_cca=True,
                    threshold=None, verbose=10, random_state=RANDOM_SEED, n_jobs=-2)
        canica._raw_fit(X)
        A = canica.components_

        # Generate ica features 
        pinvA = np.linalg.pinv(A)
        for subject_id in subjects:
            if not os.path.isfile(save_temp.format(type_id=DATA_TYPE, variant_id='task-standard', subject_id=subject_id)):
                logger.info('task-ica sd-connectome on subject ' + str(subject_id))
                img_data = np.load(data_load_temp.format(type_id=DATA_TYPE, subject_id=subject_id))
                variant_id = 'task-standard'
                F = compute_semidense_connectome(img_data, A, normalize=True)
                np.save(save_temp.format(type_id=DATA_TYPE, variant_id=variant_id, subject_id=subject_id), F)
                # variant_id = 'task-dualreg'
                # t, cope, varcope = dual_regression_glm(img_data, pinvA=pinvA)
                # F = compute_semidense_connectome(img_data, t, normalize=True)
                # np.save(save_temp.format(type_id=DATA_TYPE, variant_id=variant_id, subject_id=subject_id), F)
    ######################################################
    # MMP Feature Extraction
    if DO_MMP:
        data_load_temp = os.path.join(project_dir,'data/processed/{type_id}/{subject_id}_rest_data.npy') 
        save_temp = os.path.join(project_dir,'data/processed/features/{type_id}/mmp-379/{variant_id}_{subject_id}.npy')
        if not os.path.exists(os.path.join(project_dir,'data/processed/features/{type_id}/mmp-379').format(type_id=DATA_TYPE)):
            os.makedirs(os.path.join(project_dir,'data/processed/features/{type_id}/mmp-379').format(type_id=DATA_TYPE))
        # Load the ROIs from 91282 dim cifti
        A = np.load(os.path.join(project_dir, 'data/external/hcp_parc_379.npy'))
        pinvA = np.linalg.pinv(A)
        for subject_id in subjects:
            logger.info('mmp-379 feature ' + str(subject_id))
            img_data = np.load(data_load_temp.format(type_id=DATA_TYPE, subject_id=subject_id))
            variant_id = 'standard'
            F = compute_semidense_connectome(img_data, A, normalize=False)
            np.save(save_temp.format(type_id=DATA_TYPE, variant_id=variant_id, subject_id=subject_id), F)
            variant_id = 'dualreg'
            t, cope, varcope = dual_regression_glm(img_data, pinvA=pinvA)
            F = compute_semidense_connectome(img_data, t, normalize=False)
            np.save(save_temp.format(type_id=DATA_TYPE, variant_id=variant_id, subject_id=subject_id), F)
    ######################################################
    if DO_GRP:
        # MMP Feature Extraction
        save_temp = os.path.join(project_dir,'data/processed/features/{type_id}/GRP/{subject_id}.npy')
        if not os.path.exists(os.path.join(project_dir,'data/processed/features/{type_id}/GRP').format(type_id=DATA_TYPE)):
            os.makedirs(os.path.join(project_dir,'data/processed/features/{type_id}/GRP').format(type_id=DATA_TYPE))
        # Fix seed
        ncomponents = 379
        rs = np.random.RandomState(RANDOM_SEED)
        grp_comp = rs.normal(loc=0, scale=1.0/np.sqrt(ncomponents), size=(ncomponents, 91282))
        for subject_id in subjects:
            logger.info('GRP feature ' + str(subject_id))
            img_data = np.load(data_load_temp.format(type_id=DATA_TYPE, subject_id=subject_id))
            F = compute_semidense_connectome(img_data, grp_comp, normalize=True)
            np.save(save_temp.format(type_id=DATA_TYPE, subject_id=subject_id), F)

    ######################################################
    # AF Feature Extraction
    if DO_AFMOD:
        data_load_temp = os.path.join(project_dir,'data/processed/{type_id}/{subject_id}_rest_data.npy') 
        # Get training set subjects
        SUBJECT_CSV_NAME = os.getenv('TRAIN_SUBJECT_CSV_NAME')
        subjects_df_train = pd.read_csv(os.path.join(project_dir, 'data/external/subject_lists/', SUBJECT_CSV_NAME))
        subjects_train = subjects_df_train['Subject']

        save_temp = os.path.join(project_dir,'data/processed/features/{type_id}/AF-Mod/{subject_id}_{contrast_id}.npy')
        if not os.path.exists(os.path.join(project_dir,'data/processed/features/{type_id}/AF-Mod').format(type_id=DATA_TYPE)):
            os.makedirs(os.path.join(project_dir,'data/processed/features/{type_id}/AF-Mod').format(type_id=DATA_TYPE))
        # Load all 47 contrasts and average across subjects
        contrasts_path_temp = os.path.join(project_dir, 'data', 'processed', 'contrasts', '{subject_id}.npy')
        Y = load_contrasts(subjects_train, contrasts_path_temp)
        A = np.mean(Y, axis=0)
        for subject_id in subjects:
            # load rsfmri data
            logger.info('AF-Mod feature ' + str(subject_id))
            img_data = np.load(data_load_temp.format(type_id=DATA_TYPE, subject_id=subject_id))
            # compute AF across contrasts
            for contrast_index, contrast_id in enumerate(CONTRAST_IDS):
                F = compute_semidense_connectome(img_data, A[contrast_index], normalize=True)
                F = np.expand_dims(F, axis=0)
                np.save(save_temp.format(type_id=DATA_TYPE, contrast_id=contrast_id, subject_id=subject_id), F)
    ######################################################

if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()
