import numpy as np
from scipy import stats 
from sklearn.decomposition import PCA
from sklearn import linear_model


def compute_parpca_roi_reg(data, source_vs_mask, sink_vs, ncomponents=512, random_state=42, whiten=False):
    """Compute the partial 
        
        Parameters
        ----------
        data : numpy array (float)
               vertex x time numpy array to process
        source_vs_mask : numpy array (bool)
                         source mask to select vertices/voxels for dimensionality reduction
        sink_vs : numpy array (bool)
                  sink mask to select roi to average and regress on
        ncomponents : int
                      number of components to select from PCA (default 512)
        random_state : int
                       random seed
        whiten : bool
                 apply whitening (default False)
        Returns
        -------
        feat_array : numpy array
                     features, the coefficients projected back into the original brain space
        par_interaction_mat : numpy array
                              original model coefficient prior to mapping 
    """

    source_data = data[source_vs_mask,:]
    pca = PCA(n_components=ncomponents, random_state=random_state, whiten=whiten) # randomized PCA
    X = pca.fit_transform(source_data.T)
    #print("PCA computed...")
    # Add 'constant' regressor
    #X = np.concatenate([np.ones((1, X.shape[0])).T, X ], axis=1)
    # Save coef in matrix
    par_interaction_mat = np.zeros(ncomponents+1)
    # Regress onto sink vertices average (mean)
    Y = data[sink_vs,:]
    y = np.mean(Y, axis=0)
    RR = linear_model.LinearRegression(normalize=False, fit_intercept=True)
    RR.fit(X,y)
    par_interaction_mat[0] = RR.intercept_
    par_interaction_mat[1:] = RR.coef_

    # project back
    feat_array = pca.inverse_transform(par_interaction_mat[1:])
    return feat_array, par_interaction_mat

def glm(X,Y):
    """
    X : numpy, time x components
    Y : numpy.array, voxels x time
    """
    C = np.eye(X.shape[1])
    beta = np.dot(np.linalg.pinv(X), Y)
    cope = np.dot(C, beta)
    res = Y - np.dot(X, beta)
    dof = res.shape[0]-np.linalg.matrix_rank(X)
    sigma_sq = np.sum(np.power(res,2), axis=0)/dof
    varcope = np.dot(np.expand_dims(np.diag(np.dot(np.dot(C, np.linalg.inv(np.dot(X.T, X))), C.T)), axis=1), np.expand_dims(sigma_sq, axis=0))
    
    t = cope / np.sqrt(varcope)
    return cope, varcope, t

def dual_regression_glm(img_data, pinvA=None, A=None):
    """ img_data : time x features
    pinv_A : features x components """
    if pinvA is None:
        pinvA = np.linalg.pinv(A)
    cope, varcope, t = glm(np.dot(pinvA.T, img_data.T).T, img_data)
    #t[0:int(t.shape[0]/2), 29696:] = 0.0
    #t[int(t.shape[0]/2):, 0:29696] = 0.0
    return t, cope, varcope

def compute_semidense_connectome(X, A, normalize=True):
    """ Compute weighted Correlation Matrix
    X : t x v, A, v x f """
    if normalize:
        A = stats.zscore(A,axis=0) # Normalize features
    F = np.dot(np.dot(A, X.T), X) # (f x t) # Take weighted signal average of regions.
    F = stats.zscore(F,axis=0)
    return F


def get_contrast_names(contrasts=None):
    """Retrieve selected contrast names, 47"""
    if contrasts == 'full47':
        contrast_indx = [0,1,2,6,7,8,12,13,14,18,19,20,21,22,23,24,25,26,27,28,29,30,44,45,46,\
            50,51,52,56,57,58,59,60,61,62,63,64,65,66,70,71,72,73,74,75,76,77] 
    elif contrasts == None:
        contrast_indx = []
    contrast_ids = {'EMOTION': ['FACES', 'SHAPES', 'FACES-SHAPES',
                                            'neg_FACES', 'neg_SHAPES', 'SHAPES-FACES'],
                                'GAMBLING': ['PUNISH', 'REWARD', 'PUNISH-REWARD',
                                            'neg_PUNISH', 'neg_REWARD', 'REWARD-PUNISH'],
                                'LANGUAGE': ['MATH', 'STORY', 'MATH-STORY',
                                            'STORY-MATH', 'neg_MATH', 'neg_STORY'],
                                'MOTOR': ['CUE', 'LF', 'LH', 'RF', 'RH', 'T',
                                        'AVG', 'CUE-AVG', 'LF-AVG', 'LH-AVG', 'RF-AVG',
                                        'RH-AVG', 'T-AVG', 'neg_CUE', 'neg_LF',
                                        'neg_LH', 'neg_RF', 'neg_RH', 'neg_T',
                                        'neg_AVG', 'AVG-CUE', 'AVG-LF', 'AVG-LH',
                                        'AVG-RF', 'AVG-RH', 'AVG-T'],
                                'RELATIONAL': ['MATCH', 'REL', 'MATCH-REL',
                                                'REL-MATCH', 'neg_MATCH', 'neg_REL'],
                                'SOCIAL': ['RANDOM', 'TOM', 'RANDOM-TOM',
                                            'neg_RANDOM', 'neg_TOM', 'TOM-RANDOM'],
                                'WM': ['2BK_BODY', '2BK_FACE', '2BK_PLACE',
                                        '2BK_TOOL', '0BK_BODY', '0BK_FACE', '0BK_PLACE',
                                        '0BK_TOOL', '2BK', '0BK', '2BK-0BK',
                                        'neg_2BK', 'neg_0BK', '0BK-2BK', 'BODY',
                                        'FACE', 'PLACE', 'TOOL', 'BODY-AVG',
                                        'FACE-AVG', 'PLACE-AVG', 'TOOL-AVG', 'neg_BODY',
                                        'neg_FACE', 'neg_PLACE', 'neg_TOOL', 'AVG-BODY',
                                        'AVG-FACE', 'AVG-PLACE', 'AVG-TOOL']}
    contrast_list = sum(list(contrast_ids.values()), [])
    selected_contrasts = [contrast_list[i] for i in contrast_indx]
    return selected_contrasts


def load_features(subject_ids, feature_path_temp):
    """Loads all features into numpy array (subjects x features x v dimension"""
    X = None
    try: 
        X_sample = np.load(feature_path_temp.format(subject_id=subject_ids[0]))
        if X_sample.ndim > 1:
            n_features = X_sample.shape[0]
            v_dim = X_sample.shape[1]
            X = np.zeros((len(subject_ids), n_features, v_dim))
            for i,subject_id in enumerate(subject_ids):
                X[i,:,:] = np.load(feature_path_temp.format(subject_id=subject_id))
        else:
            X = np.zeros((len(subject_ids), X_sample.shape[0]))
            for i,subject_id in enumerate(subject_ids):
                X[i,:] = np.load(feature_path_temp.format(subject_id=subject_id))
    except Exception as e:
        print(e)
    return X

def load_contrasts(subject_ids, contrast_path_temp):
    Y = None
    try: 
        Y_sample = np.load(contrast_path_temp.format(subject_id=subject_ids[0]))
        v_dim = Y_sample.shape[1]
        Y = np.zeros((len(subject_ids), Y_sample.shape[0], v_dim))
        for i,subject_id in enumerate(subject_ids):
            Y[i,:,:] = np.load(contrast_path_temp.format(subject_id=subject_id))
    except Exception as e:
        print(e)
    return Y