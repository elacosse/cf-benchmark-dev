import numpy as np
import pandas as pd
from sklearn.metrics import r2_score

import os, sys
import argparse
#import gamma_fit as gf


def get_contrast_names(contrasts=None):
    """Retrieve selected contrast names, 47"""
    if contrasts == 'full47':
        contrast_indx = [0,1,2,6,7,8,12,13,14,18,19,20,21,22,23,24,25,26,27,28,29,30,44,45,46,\
            50,51,52,56,57,58,59,60,61,62,63,64,65,66,70,71,72,73,74,75,76,77] 
    elif contrasts == None:
        contrast_indx = []
    contrast_ids = {'EMOTION': ['FACES', 'SHAPES', 'FACES-SHAPES',
                                            'neg_FACES', 'neg_SHAPES', 'SHAPES-FACES'],
                                'GAMBLING': ['PUNISH', 'REWARD', 'PUNISH-REWARD',
                                            'neg_PUNISH', 'neg_REWARD', 'REWARD-PUNISH'],
                                'LANGUAGE': ['MATH', 'STORY', 'MATH-STORY',
                                            'STORY-MATH', 'neg_MATH', 'neg_STORY'],
                                'MOTOR': ['CUE', 'LF', 'LH', 'RF', 'RH', 'T',
                                        'AVG', 'CUE-AVG', 'LF-AVG', 'LH-AVG', 'RF-AVG',
                                        'RH-AVG', 'T-AVG', 'neg_CUE', 'neg_LF',
                                        'neg_LH', 'neg_RF', 'neg_RH', 'neg_T',
                                        'neg_AVG', 'AVG-CUE', 'AVG-LF', 'AVG-LH',
                                        'AVG-RF', 'AVG-RH', 'AVG-T'],
                                'RELATIONAL': ['MATCH', 'REL', 'MATCH-REL',
                                                'REL-MATCH', 'neg_MATCH', 'neg_REL'],
                                'SOCIAL': ['RANDOM', 'TOM', 'RANDOM-TOM',
                                            'neg_RANDOM', 'neg_TOM', 'TOM-RANDOM'],
                                'WM': ['2BK_BODY', '2BK_FACE', '2BK_PLACE',
                                        '2BK_TOOL', '0BK_BODY', '0BK_FACE', '0BK_PLACE',
                                        '0BK_TOOL', '2BK', '0BK', '2BK-0BK',
                                        'neg_2BK', 'neg_0BK', '0BK-2BK', 'BODY',
                                        'FACE', 'PLACE', 'TOOL', 'BODY-AVG',
                                        'FACE-AVG', 'PLACE-AVG', 'TOOL-AVG', 'neg_BODY',
                                        'neg_FACE', 'neg_PLACE', 'neg_TOOL', 'AVG-BODY',
                                        'AVG-FACE', 'AVG-PLACE', 'AVG-TOOL']}
    contrast_list = sum(list(contrast_ids.values()), [])
    selected_contrasts = [contrast_list[i] for i in contrast_indx]
    return selected_contrasts

def load_features(subject_ids, feature_path_temp):
    """Loads all features into numpy array (subjects x features x v dimension"""
    X = None
    try: 
        X_sample = np.load(feature_path_temp.format(subject_id=subject_ids[0]))
        if X_sample.ndim > 1:
            n_features = X_sample.shape[0]
            v_dim = X_sample.shape[1]
            X = np.zeros((len(subject_ids), n_features, v_dim))
            for i,subject_id in enumerate(subject_ids):
                X[i,:,:] = np.load(feature_path_temp.format(subject_id=subject_id))
        else:
            X = np.zeros((len(subject_ids), X_sample.shape[0]))
            for i,subject_id in enumerate(subject_ids):
                X[i,:] = np.load(feature_path_temp.format(subject_id=subject_id))
    except Exception as e:
        print(e)
    return X

def load_contrasts(subject_ids, contrast_path_temp):
    Y = None
    try: 
        Y_sample = np.load(contrast_path_temp.format(subject_id=subject_ids[0]))
        v_dim = Y_sample.shape[1]
        Y = np.zeros((len(subject_ids), Y_sample.shape[0], v_dim))
        for i,subject_id in enumerate(subject_ids):
            Y[i,:,:] = np.load(contrast_path_temp.format(subject_id=subject_id))
    except Exception as e:
        print(e)
    return Y


