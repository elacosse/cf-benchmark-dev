# -*- coding: utf-8 -*-
import click
import logging
from pathlib import Path
import pandas as pd
from dotenv import find_dotenv, load_dotenv
import os
import sys
import numpy as np
from model_utils import load_features, load_contrasts
import pickle
from sklearn import linear_model



def train_ridge_sv(X, y):
    """Trains ridge regression model from sklearn.
    X - numpy array - sd connectome - subjects x features x v dim 
    y - numpy array - glm target - subjects x v dim 
    returns : list of sklearn models """

    v_dim = X.shape[-1]
    models = dict()
    alphas = 10**np.linspace(10,-2,50)*0.5
    for v in range(0, v_dim):
        rreg = linear_model.RidgeCV(alphas=alphas, normalize=True)
        rreg.fit(X[:,:,v], y[:,v])
        models[v] = rreg
    return models

def train_ols_sv(X, y):
    """Trains ols model from sklearn.
    X - numpy array - sd connectome - subjects x features x v dim 
    y - numpy array - glm target - subjects x v dim 
    returns : list of sklearn models """
    v_dim = X.shape[-1]
    models = dict()
    for v in range(0, v_dim):
        rreg = linear_model.LinearRegression(fit_intercept=True, normalize=True)
        xv = X[:, :, v]
        yv = y[:, v]
        rreg.fit(xv,yv.reshape(-1,1))
        models[v] = rreg
    return models

def train_ridge_region(X,y,ROIS):
    roi_num = ROIS.shape[0]
    subject_num = X.shape[0]
    models = dict()
    alphas = 10**np.linspace(10,-2,50)*0.5
    for roi in range(roi_num):
        # estimate param over regions from MMP parcels
        mask = np.array(ROIS[roi, :], dtype='bool')
        Xtrroi = np.concatenate([X[i,:,mask] for i in range(0, subject_num)], axis=0)
        Ytr = np.concatenate([y[i,mask] for i in range(0, subject_num)])
        rreg = linear_model.RidgeCV(alphas=alphas, normalize=True)
        rreg.fit(Xtrroi, Ytr)
        models[roi] = rreg
    return models

@click.command()
@click.argument('yml_experiment_name')
def main(yml_experiment_name):

    """ Trains models to save into models/{model_name} directory
    """
    logger = logging.getLogger(__name__)
    logger.info('running experiment <-- ' + yml_experiment_name)
    logger.info('path to yml: ' + os.path.join(project_dir, 'src', 'experiments'))
    load_dotenv(os.path.join(project_dir, 'src', 'experiments', yml_experiment_name))

    # load environment variables for training
    HCP_BASEDIR = os.getenv('HCP_BASEDIR')
    RANDOM_SEED = int(os.getenv('RANDOM_SEED'))
    SUBJECT_CSV_NAME = os.getenv('TRAIN_SUBJECT_CSV_NAME')
    DATA_TYPE = os.getenv('DATA_TYPE')
    CONTRAST_IDS = [int(c) for c in os.getenv('CONTRAST_INDICES')[1:-1].split(',')] 
    DO_SPPCR=int(os.getenv('DO_SPPCR'))
    DO_GICA=int(os.getenv('DO_GICA'))
    DO_MMP=int(os.getenv('DO_MMP'))
    DO_GRP=int(os.getenv('DO_GRP'))
    DO_AFMOD=int(os.getenv('DO_AFMOD'))
    DO_BASELINE=int(os.getenv('DO_BASELINE'))

    # get subject ids
    subjects_df = pd.read_csv(os.path.join(project_dir, 'data/external/subject_lists/', SUBJECT_CSV_NAME))
    subjects = subjects_df['Subject']
    # Load all contrasts
    contrasts_path_temp = os.path.join(project_dir, 'data', 'processed', 'contrasts', '{subject_id}.npy')
    y = load_contrasts(subjects, contrasts_path_temp)

    if DO_GICA:
        model_name = 'Rest-Rest-GICA-RR'
        features_path_temp = os.path.join(project_dir, 'data', 'processed', 'features', DATA_TYPE, 'ica', 'standard_{subject_id}.npy') 
        X = load_features(subjects, features_path_temp)
        if not os.path.exists(os.path.join(project_dir, 'models', model_name)):
            os.makedirs(os.path.join(project_dir, 'models', model_name))
        model_save_temp = os.path.join(project_dir, 'models', model_name, '{contrast_id}-contrast-index-model.pickle')
        for contrast_index, contrast_id in enumerate(CONTRAST_IDS):
            logger.info(model_name + " training - contrast index - " + str(contrast_index))
            models = train_ridge_sv(X, y[:,contrast_index])
            with open(model_save_temp.format(contrast_id=contrast_id), 'wb') as handle:
                pickle.dump(models, handle, protocol=pickle.HIGHEST_PROTOCOL)

        model_name = 'Rest-Task-GICA-RR'
        features_path_temp = os.path.join(project_dir, 'data', 'processed', 'features', DATA_TYPE, 'ica', 'task-standard_{subject_id}.npy') 
        X = load_features(subjects, features_path_temp)
        if not os.path.exists(os.path.join(project_dir, 'models', model_name)):
            os.makedirs(os.path.join(project_dir, 'models', model_name))
        model_save_temp = os.path.join(project_dir, 'models', model_name, '{contrast_id}-contrast-index-model.pickle')
        for contrast_index, contrast_id in enumerate(CONTRAST_IDS):
            logger.info(model_name + " training - contrast index - " + str(contrast_index))
            models = train_ridge_sv(X, y[:,contrast_index])
            with open(model_save_temp.format(contrast_id=contrast_id), 'wb') as handle:
                pickle.dump(models, handle, protocol=pickle.HIGHEST_PROTOCOL)

        # model_name = 'Rest-Rest-GICA-RR-DR'
        # features_path_temp = os.path.join(project_dir, 'data', 'processed', 'features', DATA_TYPE, 'ica', 'dualreg_{subject_id}.npy') 
        # X = load_features(subjects, features_path_temp)
        # if not os.path.exists(os.path.join(project_dir, 'models', model_name)):
        #     os.makedirs(os.path.join(project_dir, 'models', model_name))
        # model_save_temp = os.path.join(project_dir, 'models', model_name, '{contrast_id}-contrast-index-model.pickle')
        # for contrast_index, contrast_id in enumerate(CONTRAST_IDS):
        #     logger.info(model_name + " training - contrast index - " + str(contrast_index))
        #     models = train_ridge_sv(X, y[:,contrast_index])
        #     with open(model_save_temp.format(contrast_id=contrast_id), 'wb') as handle:
        #         pickle.dump(models, handle, protocol=pickle.HIGHEST_PROTOCOL)

    if DO_GRP:
        model_name = 'GRP-RR'
        features_path_temp = os.path.join(project_dir, 'data', 'processed', 'features', DATA_TYPE, 'GRP', '{subject_id}.npy') 
        X = load_features(subjects, features_path_temp)
        if not os.path.exists(os.path.join(project_dir, 'models', model_name)):
            os.makedirs(os.path.join(project_dir, 'models', model_name))
        model_save_temp = os.path.join(project_dir, 'models', model_name, '{contrast_id}-contrast-index-model.pickle')
        for contrast_index, contrast_id in enumerate(CONTRAST_IDS):
            logger.info(model_name + " training - contrast index - " + str(contrast_index))
            models = train_ridge_sv(X, y[:,contrast_index])
            with open(model_save_temp.format(contrast_id=contrast_id), 'wb') as handle:
                pickle.dump(models, handle, protocol=pickle.HIGHEST_PROTOCOL)        
    
    if DO_MMP:
        A = np.load(os.path.join(project_dir, 'data/external/hcp_parc_379.npy'))
        model_name = 'Rest-MMP-ParcelRR'
        features_path_temp = os.path.join(project_dir, 'data', 'processed', 'features', DATA_TYPE, 'mmp-379', 'standard_{subject_id}.npy') 
        X = load_features(subjects, features_path_temp)
        if not os.path.exists(os.path.join(project_dir, 'models', model_name)):
            os.makedirs(os.path.join(project_dir, 'models', model_name))
        model_save_temp = os.path.join(project_dir, 'models', model_name, '{contrast_id}-contrast-index-model.pickle')
        for contrast_index, contrast_id in enumerate(CONTRAST_IDS):
            logger.info(model_name + " training - contrast index - " + str(contrast_index))
            models = train_ridge_region(X, y[:,contrast_index], A)
            with open(model_save_temp.format(contrast_id=contrast_id), 'wb') as handle:
                pickle.dump(models, handle, protocol=pickle.HIGHEST_PROTOCOL)

        model_name = 'Rest-MMP-RR'
        features_path_temp = os.path.join(project_dir, 'data', 'processed', 'features', DATA_TYPE, 'mmp-379', 'standard_{subject_id}.npy') 
        X = load_features(subjects, features_path_temp)
        if not os.path.exists(os.path.join(project_dir, 'models', model_name)):
            os.makedirs(os.path.join(project_dir, 'models', model_name))
        model_save_temp = os.path.join(project_dir, 'models', model_name, '{contrast_id}-contrast-index-model.pickle')
        for contrast_index, contrast_id in enumerate(CONTRAST_IDS):
            logger.info(model_name + " training - contrast index - " + str(contrast_index))
            models = train_ridge_sv(X, y[:,contrast_index])
            with open(model_save_temp.format(contrast_id=contrast_id), 'wb') as handle:
                pickle.dump(models, handle, protocol=pickle.HIGHEST_PROTOCOL)

        model_name = 'Rest-MMP-RR-DR'
        features_path_temp = os.path.join(project_dir, 'data', 'processed', 'features', DATA_TYPE, 'mmp-379', 'dualreg_{subject_id}.npy') 
        X = load_features(subjects, features_path_temp)
        if not os.path.exists(os.path.join(project_dir, 'models', model_name)):
            os.makedirs(os.path.join(project_dir, 'models', model_name))
        model_save_temp = os.path.join(project_dir, 'models', model_name, '{contrast_id}-contrast-index-model.pickle')
        for contrast_index, contrast_id in enumerate(CONTRAST_IDS):
            logger.info(model_name + " training - contrast index - " + str(contrast_index))
            models = train_ridge_sv(X, y[:,contrast_index])
            with open(model_save_temp.format(contrast_id=contrast_id), 'wb') as handle:
                pickle.dump(models, handle, protocol=pickle.HIGHEST_PROTOCOL)

        model_name = 'Rest-MMP-OLS'
        features_path_temp = os.path.join(project_dir, 'data', 'processed', 'features', DATA_TYPE, 'mmp-379', 'standard_{subject_id}.npy') 
        X = load_features(subjects, features_path_temp)
        if not os.path.exists(os.path.join(project_dir, 'models', model_name)):
            os.makedirs(os.path.join(project_dir, 'models', model_name))
        model_save_temp = os.path.join(project_dir, 'models', model_name, '{contrast_id}-contrast-index-model.pickle')
        for contrast_index, contrast_id in enumerate(CONTRAST_IDS):
            logger.info(model_name + " training - contrast index - " + str(contrast_index))
            models = train_ols_sv(X, y[:,contrast_index])
            with open(model_save_temp.format(contrast_id=contrast_id), 'wb') as handle:
                pickle.dump(models, handle, protocol=pickle.HIGHEST_PROTOCOL)

    if DO_SPPCR:
        model_name = 'MMP-RR-PCR'
        A = np.load(os.path.join(project_dir, 'data/external/hcp_parc_379.npy'))
        features_path_temp = os.path.join(project_dir, 'data', 'processed', 'features', DATA_TYPE, 'sppcr', '{subject_id}.npy') 
        X = load_features(subjects, features_path_temp)
        if not os.path.exists(os.path.join(project_dir, 'models', model_name)):
            os.makedirs(os.path.join(project_dir, 'models', model_name))
        model_save_temp = os.path.join(project_dir, 'models', model_name, '{contrast_id}-contrast-index-model.pickle')
        for contrast_index, contrast_id in enumerate(CONTRAST_IDS):
            logger.info(model_name + " training - contrast index - " + str(contrast_index))
            models = train_ridge_region(X, y[:,contrast_index], A)
            with open(model_save_temp.format(contrast_id=contrast_id), 'wb') as handle:
                pickle.dump(models, handle, protocol=pickle.HIGHEST_PROTOCOL)

    if DO_AFMOD:
        model_name = 'AF-Mod'
        if not os.path.exists(os.path.join(project_dir, 'models', model_name)):
            os.makedirs(os.path.join(project_dir, 'models', model_name))
        model_save_temp = os.path.join(project_dir, 'models', model_name, '{contrast_id}-contrast-index-model.pickle')
        for contrast_index, contrast_id in enumerate(CONTRAST_IDS):
            features_path_temp = os.path.join(project_dir, 'data', 'processed', 
                                             'features', DATA_TYPE, 'AF-Mod', '{subject_id}_' + str(contrast_id) + '.npy')
            X = load_features(subjects, features_path_temp)
            logger.info(model_name + " training - contrast index - " + str(contrast_index))
            models = train_ols_sv(X, y[:,contrast_index])
            with open(model_save_temp.format(contrast_id=contrast_id), 'wb') as handle:
                pickle.dump(models, handle, protocol=pickle.HIGHEST_PROTOCOL)


    if DO_BASELINE:
        model_name = 'Anatomical-RR'
        features_path_temp = os.path.join(project_dir, 'data', 'processed', 'anatomy', '{subject_id}.npy') 
        X = load_features(subjects, features_path_temp)
        if not os.path.exists(os.path.join(project_dir, 'models', model_name)):
            os.makedirs(os.path.join(project_dir, 'models', model_name))
        model_save_temp = os.path.join(project_dir, 'models', model_name, '{contrast_id}-contrast-index-model.pickle')
        for contrast_index, contrast_id in enumerate(CONTRAST_IDS):
            logger.info(model_name + " training - contrast index - " + str(contrast_index))
            models = train_ridge_sv(X, y[:,contrast_index])
            with open(model_save_temp.format(contrast_id=contrast_id), 'wb') as handle:
                pickle.dump(models, handle, protocol=pickle.HIGHEST_PROTOCOL)

if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()

