# -*- coding: utf-8 -*-
import click
import logging
from pathlib import Path
import pandas as pd
from dotenv import find_dotenv, load_dotenv
import os, sys
import numpy as np
#from model_utils import load_features, load_contrasts, get_contrast_names, evaluate_predictions, get_threshold_scores
from model_utils import load_features, load_contrasts, get_contrast_names
import pickle
from sklearn import linear_model

def test_ridge_sv(X, model):
    """Test ridge regression model from sklearn.
    X - numpy array - sd connectome - subjects x features x v dim 
    model - array - glm target - subjects x v dim 
    returns : yhat, alpha """

    v_dim = X.shape[-1]
    yhat = np.zeros_like(X[:,0,:])
    alpha = np.zeros_like(X[0,0,:])
    for v in range(0, v_dim):
        estimator = model[v]
        yhat[:,v] = estimator.predict(X[:,:,v])
        alpha[v] = estimator.alpha_
    return yhat, alpha

def test_ridge_region(X,model,ROIS):
    """Test region-wise ridge regression model from sklearn.
    X - numpy array - sd connectome - subjects x features x v dim 
    model - array - glm target - subjects x v dim 
    returns : yhat, alpha """
    roi_num = ROIS.shape[0]
    subject_num = X.shape[0]
    yhat = np.zeros_like(X[:,0,:])
    alpha = np.zeros(roi_num)
    for roi in range(roi_num):
        estimator = model[roi]
        # estimate param over regions from MMP parcels
        mask = np.array(ROIS[roi, :], dtype='bool')
        Xroi = np.concatenate([X[i,:,mask] for i in range(0, subject_num)], axis=0)
        yhat_roi = estimator.predict(Xroi)
        yhat[:,mask] = np.reshape(yhat_roi, (subject_num, mask.sum()))
    return yhat, alpha

def test_ols_sv(X, model):
    """OLS Regression model from sklearn.
    X - numpy array - sd connectome - subjects x features x v dim 
    model - array - glm target - subjects x v dim 
    returns : yhat """

    v_dim = X.shape[-1]
    yhat = np.zeros_like(X[:,0,:])
    for v in range(0, v_dim):
        estimator = model[v]
        xv = X[:, :, v]
        yhat[:,v] = estimator.predict(xv)[:,0]
    return yhat



        
@click.command()
@click.argument('yml_experiment_name')
def main(yml_experiment_name):

    """ Runs data processing scripts to turn processed data from (../processed) into
        cleaned data ready to be analyzed (saved in ../processed/features).

        Assumes processed data in ../../data/{type_id}/{subject_id}.npy
    """
    logger = logging.getLogger(__name__)
    logger.info('running experiment <-- ' + yml_experiment_name)
    logger.info('path to yml: ' + os.path.join(project_dir, 'src', 'experiments'))
    load_dotenv(os.path.join(project_dir, 'src', 'experiments', yml_experiment_name))

    # load environment variables for training
    HCP_BASEDIR = os.getenv('HCP_BASEDIR')
    RANDOM_SEED = int(os.getenv('RANDOM_SEED'))
    SUBJECT_CSV_NAME = os.getenv('TEST_SUBJECT_CSV_NAME')
    DATA_TYPE = os.getenv('DATA_TYPE')
    CONTRAST_IDS = [int(c) for c in os.getenv('CONTRAST_INDICES')[1:-1].split(',')]
    # get subject ids
    subjects_df = pd.read_csv(os.path.join(project_dir, 'data/external/subject_lists/', SUBJECT_CSV_NAME))
    subjects = subjects_df['Subject']

    # For mean baseline model
    TRAIN_SUBJECT_CSV_NAME = os.getenv('TRAIN_SUBJECT_CSV_NAME')
    train_subjects_df = pd.read_csv(os.path.join(project_dir, 'data/external/subject_lists/', SUBJECT_CSV_NAME))
    train_subjects = train_subjects_df['Subject']

    # Load all contrasts
    contrasts_path_temp = os.path.join(project_dir, 'data', 'processed', 'contrasts', '{subject_id}.npy')
    Y_test = load_contrasts(subjects, contrasts_path_temp)

    DO_SPPCR=int(os.getenv('DO_SPPCR'))
    DO_GICA=int(os.getenv('DO_GICA'))
    DO_MMP=int(os.getenv('DO_MMP'))
    DO_GRP=int(os.getenv('DO_GRP'))
    DO_AF=int(os.getenv('DO_AF'))
    DO_AFMOD=int(os.getenv('DO_AFMOD'))
    DO_BASELINE=int(os.getenv('DO_BASELINE'))

    # get contrast names
    contrast_names = get_contrast_names(contrasts='full47')

    yhat_temp = os.path.join(project_dir, 'data/predictions/{model_name}/yhat_contrastid-{contrast_id}.npy')
    if DO_MMP == True:

        model_name = 'Rest-MMP-OLS'
        if not os.path.exists(os.path.join(project_dir, 'data/predictions', model_name)):
            os.makedirs(os.path.join(project_dir, 'data/predictions', model_name))
        features_path_temp = os.path.join(project_dir, 'data', 'processed', 'features', DATA_TYPE, 'mmp-379', 'standard_{subject_id}.npy') 
        X = load_features(subjects, features_path_temp)
        model_load_temp = os.path.join(project_dir, 'models', model_name, '{contrast_index}-contrast-index-model.pickle')
        for contrast_index, contrast_id in enumerate(CONTRAST_IDS):
            logger.info(model_name + " test - contrast index - " + str(contrast_index))
            contrast_label = contrast_names[contrast_index]
            with open(model_load_temp.format(contrast_index=contrast_id), 'rb') as handle:
                models = pickle.load(handle)
                yhat = test_ols_sv(X, models)
                np.save(yhat_temp.format(model_name=model_name, contrast_id=contrast_id), yhat)

        model_name = 'Rest-MMP-RR'
        if not os.path.exists(os.path.join(project_dir, 'data/predictions', model_name)):
            os.makedirs(os.path.join(project_dir, 'data/predictions', model_name))
        features_path_temp = os.path.join(project_dir, 'data', 'processed', 'features', DATA_TYPE, 'mmp-379', 'standard_{subject_id}.npy') 
        X = load_features(subjects, features_path_temp)
        model_load_temp = os.path.join(project_dir, 'models', model_name, '{contrast_index}-contrast-index-model.pickle')
        for contrast_index, contrast_id in enumerate(CONTRAST_IDS):
            logger.info(model_name + " test - contrast index - " + str(contrast_index))
            contrast_label = contrast_names[contrast_index]
            with open(model_load_temp.format(contrast_index=contrast_id), 'rb') as handle:
                models = pickle.load(handle)
                yhat, alpha = test_ridge_sv(X, models)
                np.save(yhat_temp.format(model_name=model_name, contrast_id=contrast_id), yhat)
        ###########################################################################################################################################
        model_name = 'Rest-MMP-ParcelRR'
        if not os.path.exists(os.path.join(project_dir, 'data/predictions', model_name)):
            os.makedirs(os.path.join(project_dir, 'data/predictions', model_name))
        A = np.load(os.path.join(project_dir, 'data/external/hcp_parc_379.npy'))
        features_path_temp = os.path.join(project_dir, 'data', 'processed', 'features', DATA_TYPE, 'mmp-379', 'standard_{subject_id}.npy') 
        X = load_features(subjects, features_path_temp)
        model_load_temp = os.path.join(project_dir, 'models', model_name, '{contrast_index}-contrast-index-model.pickle')
        for contrast_index, contrast_id in enumerate(CONTRAST_IDS):
            logger.info(model_name + " test - contrast index - " + str(contrast_index))
            contrast_label = contrast_names[contrast_index]
            with open(model_load_temp.format(contrast_index=contrast_id), 'rb') as handle:
                models = pickle.load(handle)
                yhat, alpha = test_ridge_region(X, models, A)
                np.save(yhat_temp.format(model_name=model_name, contrast_id=contrast_id), yhat)
        ###########################################################################################################################################
        model_name = 'Rest-MMP-RR-DR'
        if not os.path.exists(os.path.join(project_dir, 'data/predictions', model_name)):
            os.makedirs(os.path.join(project_dir, 'data/predictions', model_name))
        features_path_temp = os.path.join(project_dir, 'data', 'processed', 'features', DATA_TYPE, 'mmp-379', 'dualreg_{subject_id}.npy') 
        X = load_features(subjects, features_path_temp)
        model_load_temp = os.path.join(project_dir, 'models', model_name, '{contrast_index}-contrast-index-model.pickle')
        for contrast_index, contrast_id in enumerate(CONTRAST_IDS):
            logger.info(model_name + " test - contrast index - " + str(contrast_index))
            contrast_label = contrast_names[contrast_index]
            with open(model_load_temp.format(contrast_index=contrast_id), 'rb') as handle:
                models = pickle.load(handle)
                yhat, alpha = test_ridge_sv(X, models)
                np.save(yhat_temp.format(model_name=model_name, contrast_id=contrast_id), yhat)
    ###########################################################################################################################################
    if DO_SPPCR:
        model_name = 'MMP-PCR-RR'
        features_path_temp = os.path.join(project_dir, 'data', 'processed', 'features', DATA_TYPE, 'sppcr', '{subject_id}.npy') 
        X = load_features(subjects, features_path_temp)
        model_load_temp = os.path.join(project_dir, 'models', model_name, '{contrast_index}-contrast-index-model.pickle')
        for contrast_index, contrast_id in enumerate(CONTRAST_IDS):
            logger.info(model_name + " test - contrast index - " + str(contrast_index))
            contrast_label = contrast_names[contrast_index]
            with open(model_load_temp.format(contrast_index=contrast_id), 'rb') as handle:
                models = pickle.load(handle)
                yhat, alpha = test_ridge_sv(X, models)
                yhat = yhat[:,0:59412] # Surface only
                np.save(yhat_temp.format(model_name=model_name, contrast_id=contrast_id), yhat)
    ###########################################################################################################################################
    if DO_GICA:
        model_name = 'Rest-Rest-GICA-RR'
        if not os.path.exists(os.path.join(project_dir, 'data/predictions', model_name)):
            os.makedirs(os.path.join(project_dir, 'data/predictions', model_name))
        features_path_temp = os.path.join(project_dir, 'data', 'processed', 'features', DATA_TYPE, 'ica', 'standard_{subject_id}.npy') 
        X = load_features(subjects, features_path_temp)
        model_load_temp = os.path.join(project_dir, 'models', model_name, '{contrast_index}-contrast-index-model.pickle')
        for contrast_index, contrast_id in enumerate(CONTRAST_IDS):
            logger.info(model_name + " test - contrast index - " + str(contrast_index))
            contrast_label = contrast_names[contrast_index]
            with open(model_load_temp.format(contrast_index=contrast_id), 'rb') as handle:
                models = pickle.load(handle)
                yhat, alpha = test_ridge_sv(X, models)
                np.save(yhat_temp.format(model_name=model_name, contrast_id=contrast_id), yhat)

        model_name = 'Rest-Task-GICA-RR'
        if not os.path.exists(os.path.join(project_dir, 'data/predictions', model_name)):
            os.makedirs(os.path.join(project_dir, 'data/predictions', model_name))
        features_path_temp = os.path.join(project_dir, 'data', 'processed', 'features', DATA_TYPE, 'ica', 'task-standard_{subject_id}.npy') 
        X = load_features(subjects, features_path_temp)
        model_load_temp = os.path.join(project_dir, 'models', model_name, '{contrast_index}-contrast-index-model.pickle')
        for contrast_index, contrast_id in enumerate(CONTRAST_IDS):
            logger.info(model_name + " test - contrast index - " + str(contrast_index))
            contrast_label = contrast_names[contrast_index]
            with open(model_load_temp.format(contrast_index=contrast_id), 'rb') as handle:
                models = pickle.load(handle)
                yhat, alpha = test_ridge_sv(X, models)
                np.save(yhat_temp.format(model_name=model_name, contrast_id=contrast_id), yhat)
    ###########################################################################################################################################
    if DO_AF:
        model_name = 'AF'
        logger.info('running ' + model_name)
        if not os.path.exists(os.path.join(project_dir, 'data/predictions', model_name)):
            os.makedirs(os.path.join(project_dir, 'data/predictions', model_name))

        # Compute train subjects activation patterns
        SUBJECT_CSV_NAME = os.getenv('TRAIN_SUBJECT_CSV_NAME')
        train_subjects_df = pd.read_csv(os.path.join(project_dir, 'data/external/subject_lists/', SUBJECT_CSV_NAME))
        train_subjects = train_subjects_df['Subject']
        Y_train = load_contrasts(train_subjects, contrasts_path_temp)
        A = Y_train[:,:,0:59412].mean(axis=0)

        # create conjunction mask between ROI from parcellation and 9mm distance
        # exclude all vertices within same function region 
        # AND exclude all vertices within 9mm functional region as the to-be-predicted vertex
        exclusion_dist = 9 # mm
        ROIs = np.load(os.path.join(project_dir, 'data/external/hcp_parc_379.npy'))
        ROIs = ROIs[0:360, 0:59412] # surface only
        #distance = np.load(os.path.join(project_dir, 'data/interim/distance/', 'cortical_distance.npy'))
        distance = np.load('/mnt/50tbd/HCP_derivatives/cortical_distances/200TRTE_surface_midthickness_cortex_distance.npy')
        v_mask = (distance <= exclusion_dist) & (distance > 0)
        v_mask[np.diag_indices_from(v_mask)] = True
        v_mask = ~v_mask
        for roi in range(ROIs.shape[0]):
            for v in np.where(ROIs[roi] == 1)[0]:
                v_mask[v][np.where(ROIs[roi] == 1)[0]] = False
        del distance, ROIs # cleanup

        # compute AF for each subject and then all contrasts
        for i, subject in enumerate(subjects):
            X = np.load('/mnt/50tbd/HCP_derivatives/rest_task_mapping/rest/{subj_id}_rest_data.npy'.format(subj_id=subject))
            #X = np.load(os.path.join(project_dir,'data/processed/rsfmri/{subj_id}_rest_data.npy'.format(subj_id=subject)))
            logger.info(model_name + " test - subject " + str(subject))
            # calculate massive correlation matrix and Fisher transform
            W = np.corrcoef(X[:,0:59412].T)
            W = np.arctanh(W)
            W = np.nan_to_num(W, copy=False)
            # compute dot product of masked corr mat and averaged activity pattern to assign for each vertex v
            yhat_subject = np.zeros((len(CONTRAST_IDS), 59412))
            for contrast_index, contrast_id in enumerate(CONTRAST_IDS):
                logger.info(model_name + " test - contrast index - " + str(contrast_index))
                for v in range(W.shape[0]):
                    yhat_subject[contrast_index, v] = np.dot(A[contrast_index,v_mask[v]], W[v, v_mask[v]])
                np.save(os.path.join(project_dir, 'data/predictions', model_name, '{subj_id}.npy').format(subj_id=subject), yhat_subject)
            # take enormous amounts of time so save predictions?
            np.save(os.path.join(project_dir, 'data/predictions', model_name, '{subj_id}.npy').format(subj_id=subject), yhat_subject)
            del W  
        # reorganize into contrast-wise files for later evaluation
        for contrast_index, contrast_id in enumerate(CONTRAST_IDS):
            logger.info(model_name + " saving into contrast files - " + str(contrast_id))
            yhat = np.zeros((len(subjects), 59412))
            for i, subject in enumerate(subjects):   
                yhat_subject = np.load(os.path.join('/mnt/50tbd/eric/cf_benchmark_dev/data_features_predictions_SciRep2021Paper/predictions/AF/', 'AF_{subj_id}.npy').format(subj_id=subject)) 
                yhat[i] = yhat_subject[contrast_index]
            np.save(yhat_temp.format(model_name=model_name, contrast_id=contrast_id), yhat)
    ###########################################################################################################################################
    if DO_AFMOD:
        model_name = 'AF-Mod'
        if not os.path.exists(os.path.join(project_dir, 'data/predictions', model_name)):
            os.makedirs(os.path.join(project_dir, 'data/predictions', model_name))

        model_load_temp = os.path.join(project_dir, 'models', model_name, '{contrast_index}-contrast-index-model.pickle')

        for contrast_index, contrast_id in enumerate(CONTRAST_IDS):
            features_path_temp = os.path.join(project_dir, 'data', 'processed', 
                                             'features', DATA_TYPE, model_name, '{subject_id}_' + str(contrast_index) + '.npy')
            X = load_features(subjects, features_path_temp)
            logger.info(model_name + " testing - contrast index - " + str(contrast_index))
            with open(model_load_temp.format(contrast_index=contrast_id), 'rb') as handle:
                models = pickle.load(handle)
                yhat = test_ols_sv(X, models)
                np.save(yhat_temp.format(model_name=model_name, contrast_id=contrast_id), yhat)

    ###########################################################################################################################################
    if DO_BASELINE:
    ###########################################################################################################################################
        model_name = 'Mean-Baseline'
        if not os.path.exists(os.path.join(project_dir, 'data/predictions', model_name)):
            os.makedirs(os.path.join(project_dir, 'data/predictions', model_name))
        # average across subjects
        contrasts_path_temp = os.path.join(project_dir, 'data', 'processed', 'contrasts', '{subject_id}.npy')
        y_train = load_contrasts(train_subjects, contrasts_path_temp)
        yhat = np.tile(y_train[:,:,:].mean(axis=0), (100,1,1))
        for contrast_index, contrast_id in enumerate(CONTRAST_IDS):
            logger.info(model_name + " test - contrast index - " + str(contrast_index))
            np.save(yhat_temp.format(model_name=model_name, contrast_id=contrast_id), yhat)

        ##########################################################################################################################################
        model_name = 'Anatomical-RR'
        if not os.path.exists(os.path.join(project_dir, 'data/predictions', model_name)):
            os.makedirs(os.path.join(project_dir, 'data/predictions', model_name))
        features_path_temp = os.path.join(project_dir, 'data', 'processed', 'anatomy', '{subject_id}.npy') 
        X = load_features(subjects, features_path_temp)
        model_load_temp = os.path.join(project_dir, 'models', model_name, '{contrast_index}-contrast-index-model.pickle')
        for contrast_index, contrast_id in enumerate(CONTRAST_IDS):
            logger.info(model_name + " test - contrast index - " + str(contrast_index))
            contrast_label = contrast_names[contrast_index]
            with open(model_load_temp.format(contrast_index=contrast_id), 'rb') as handle:
                models = pickle.load(handle)
                yhat, alpha = test_ridge_sv(X, models)
                np.save(yhat_temp.format(model_name=model_name, contrast_id=contrast_id), yhat)
if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()

