# -*- coding: utf-8 -*-
import click
import logging
from pathlib import Path
import pandas as pd
from dotenv import find_dotenv, load_dotenv
import os
import sys
import numpy as np
from eval_utils import load_contrasts, get_contrast_names, evaluate_predictions

def save_eval_report(yhats, Y_test, ggm_thresholds, model_name, subjects, contrast_names, CONTRAST_IDS):
    # evaluate predictions
    dfr = []
    dfr2 = []
    dfthr = [] 
    for contrast_index, contrast_id in enumerate(CONTRAST_IDS):
        contrast_label = contrast_names[contrast_index]
        subject_ggm_thr = ggm_thresholds[:,contrast_index]
        yhat = yhats[:,contrast_index,0:59412]
        y = Y_test[:,contrast_index,0:59412]
        dfrc,dfr2c = evaluate_predictions(subjects, yhat, y, contrast_label, contrast_id, model_name)
        dfthrc = get_threshold_scores(subjects, yhat, y, contrast_label, contrast_id, model_name, subject_ggm_thr)
        dfr.append(dfrc)
        dfr2.append(dfr2c)
        dfthr.append(dfthrc)
    dfr = pd.concat(dfr)
    dfr2 = pd.concat(dfr2)
    dfthr = pd.concat(dfthr)
    # Save model to report
    dfr.to_csv(os.path.join(project_dir,'reports', 'model-evaluation', 'r', model_name +'.csv'))
    dfr2.to_csv(os.path.join(project_dir,'reports', 'model-evaluation', 'r2', model_name+'.csv'))
    dfthr.to_csv(os.path.join(project_dir,'reports', 'model-evaluation', 'threshold', model_name+'.csv'))


@click.command()
@click.argument('yml_experiment_name')
def main(yml_experiment_name):

    """ Runs data processing scripts to turn processed data from (../processed) into
        cleaned data ready to be analyzed (saved in ../processed/features).

        Assumes processed data in ../../data/{type_id}/{subject_id}.npy
    """
    logger = logging.getLogger(__name__)
    logger.info('running experiment <-- ' + yml_experiment_name)
    logger.info('path to yml: ' + os.path.join(project_dir, 'src', 'experiments'))
    load_dotenv(os.path.join(project_dir, 'src', 'experiments', yml_experiment_name))

    # load environment variables
    HCP_BASEDIR = os.getenv('HCP_BASEDIR')
    RANDOM_SEED = int(os.getenv('RANDOM_SEED'))
    SUBJECT_CSV_NAME = os.getenv('TEST_SUBJECT_CSV_NAME')
    DATA_TYPE = os.getenv('DATA_TYPE')
    CONTRAST_IDS = os.getenv('CONTRAST_INDICES')[1:-1].split(',')
    SURFACE_EVALUATION_ONLY = int(os.getenv('SURFACE_EVALUATION_ONLY'))
    contrast_names = get_contrast_names(contrasts='full47')
    # get subject ids
    subjects_df = pd.read_csv(os.path.join(project_dir, 'data/external/subject_lists/', SUBJECT_CSV_NAME))
    subjects = subjects_df['Subject']

    # Find all directories in predictions/*
    all_subdirs = [d for d in os.listdir(os.path.join(project_dir, 'data/predictions'))]

    # # Load Contrasts
    contrasts_path_temp = os.path.join(project_dir, 'data', 'processed', 'contrasts', '{subject_id}.npy')
    logging.info('loading target contrasts ... ')
    Y_test = load_contrasts(subjects, contrasts_path_temp)

    # Check if GGM thresholds are precomputed
    try:
        subject_ggm_thrs_pos = np.load(os.path.join(project_dir, 
            'data', 'interim', 'pos_GGM_thresholds_100_train_100_test_TEST.npy')) # 100 x all 47
    except Exception as e:
        subject_ggm_thrs_pos = None
        logging.info('unable to load precomputed positive GGM thresholds')
    # Load Predictions
    for model_name in all_subdirs:
        logging.info('evaluating model - ' + model_name)
        prediction_temp = os.path.join(project_dir, 'data/predictions', model_name, 'yhat_contrastid-{contrast_id}.npy')
        #prediction_temp = os.path.join(project_dir, 'data/predictions', model_name, 'ytehat_contrast_{contrast_id}.npy')
        dfr = []
        dfr2 = []
        dfthr = []
        for contrast_index, contrast_id in enumerate(CONTRAST_IDS):
            try:
                yhat = np.load(prediction_temp.format(contrast_id=contrast_id))
                if SURFACE_EVALUATION_ONLY:
                    yhat = yhat[:,0:59412]
                    y = Y_test[:,contrast_index, 0:59412]
                else: y = Y_test[:,contrast_index, :]
                
                logging.info('evaluating contrast - ' + str(contrast_id) + ' - ' + contrast_names[contrast_index])
                dfrc, dfr2c, dfthrc = evaluate_predictions(subjects, yhat, y,
                     contrast_names[contrast_index], contrast_id, model_name, subject_ggm_thrs_pos[:,contrast_index])
                dfr.append(dfrc)
                dfr2.append(dfr2c)
                dfthr.append(dfthrc)
            except Exception as e:
                print(e)
        if len(dfr) > 0:
            dfr = pd.concat(dfr)
            dfr2 = pd.concat(dfr2)
            dfthr = pd.concat(dfthr)
            # Save model to report
            dfr.to_csv(os.path.join(project_dir,'reports', 'model-evaluation', 'r', model_name +'.csv'))
            dfr2.to_csv(os.path.join(project_dir,'reports', 'model-evaluation', 'r2', model_name+'.csv'))
            dfthr.to_csv(os.path.join(project_dir,'reports', 'model-evaluation', 'threshold', model_name+'.csv'))
            logging.info('... saved results - ' + model_name + ' ...')

if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()