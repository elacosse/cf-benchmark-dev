import gamma_fit as gf
import sys
from copy import deepcopy

import numpy as np
import pandas as pd
from sklearn.metrics import r2_score

import os, sys
import argparse
#import gamma_fit as gf

def fit_model(masked_data, components, label, plot=False):
        
    em = gf.EM(components)
    em.fit(masked_data)
    gamma_gauss_pp = em.posteriors(masked_data)
    bic = em.BIC(masked_data)

    # if plot:
    #     from mpl_toolkits.axes_grid.anchored_artists import AnchoredText
    #     fig = plt.figure()
    #     ax = fig.add_subplot(111)
    #     plt.hist(masked_data, bins=50, normed=True)
    #     xRange = np.arange(np.floor(min(masked_data)), np.ceil(max(masked_data)), 0.1)
    #     pdf_sum = np.zeros(xRange.shape)
    #     for i, component in enumerate(em.components):
    #         pdf = component.pdf(xRange) * em.mix[i]
    #         plt.plot(xRange, pdf)
    #         pdf_sum += pdf

    #     at = AnchoredText("BIC = %f" % (bic),
    #               loc=1, prop=dict(size=8), frameon=True,
    #               )
    #     at.patch.set_boxstyle("round,pad=0.,rounding_size=0.2")
    #     ax.add_artist(at)
    #     plt.plot(xRange, pdf_sum)
    #     plt.xlabel("T values")
    #     #plt.savefig("histogram%s.pdf" % label)
        
    #     return gamma_gauss_pp, bic, ax

    return gamma_gauss_pp, bic

def apply_gamma_fit(data, models):
    """Applies a Gamma Gaussian Model to find thresholds on data"""
    
    #fname = self.inputs.stat_image
    #img = nifti.load(fname)
    #data = np.array(img.get_data())
    #masked_data = data[mask > 0].ravel().squeeze()

    no_signal_components = [gf.GaussianComponent(0, 10)]
    no_signal_zero_components = [gf.FixedMeanGaussianComponent(0, 10)]

    noise_and_activation_components = deepcopy(no_signal_components)
    noise_and_activation_components.append(gf.GammaComponent(4, 5))
    
    noise_zero_and_activation_components = deepcopy(no_signal_zero_components)
    noise_zero_and_activation_components.append(gf.GammaComponent(4, 5))
    
    noise_and_deactivation_components = deepcopy(no_signal_components)
    noise_and_deactivation_components.append(gf.NegativeGammaComponent(4, 5))

    noise_activation_and_deactivation_components = [gf.NegativeGammaComponent(4, 5)] + deepcopy(noise_and_activation_components)
    noise_zero_activation_and_deactivation_components = [gf.NegativeGammaComponent(4, 5)] + deepcopy(noise_zero_and_activation_components)

    best = (None, sys.maxsize, None)
    
    for model in models:
        components = {'no_signal': no_signal_components,
                      'no_signal_zero': no_signal_zero_components,
                      'noise_and_activation': noise_and_activation_components,
                      'noise_and_deactivation': noise_and_deactivation_components,
                      'noise_zero_and_activation': noise_zero_and_activation_components,
                      'noise_activation_and_deactivation': noise_activation_and_deactivation_components,
                      'noise_zero_activation_and_deactivation': noise_zero_activation_and_deactivation_components}
        #gamma_gauss_pp, bic, ax = fit_model(data, components[model], label=model, plot=True)
        gamma_gauss_pp, bic = fit_model(data, components[model], label=model, plot=False)
        if bic < best[1]:
            best = (gamma_gauss_pp, bic, model)
        
    gamma_gauss_pp = best[0]
    selected_model = best[2] ###
    thresholded_map = np.zeros(data.shape)

    active_map = np.zeros(data.shape) == 1
    if selected_model.endswith('activation_and_deactivation'):
        active_map = np.logical_and(gamma_gauss_pp[:, 2] > gamma_gauss_pp[:, 1], np.logical_and(gamma_gauss_pp[:, 2] > gamma_gauss_pp[:, 0], data > 0.01))
        gaussian_mean = components[selected_model][1].mu ###
        
        #deactive_map = np.logical
    elif selected_model.endswith('and_activation'):
        active_map = np.logical_and(gamma_gauss_pp[:, 1] > gamma_gauss_pp[:, 0], data > 0.01)
        gaussian_mean = components[selected_model][0].mu
#     elif selected_model.endswith('and_deactivation'):
#         active_map = None
#         gaussian_mean = None
    else:
        gaussian_mean = components[selected_model][0].mu

    thresholded_map[active_map] = data[active_map]
    if active_map.sum() != 0:
        threshold = data[active_map].min()
    else:
        threshold = data.max() + 1 #setting artificially high threshold


    return active_map, thresholded_map, threshold, selected_model
        
def confused(x, y, x_thr, y_thr):
    """Computes some confusion matrix items that compares two contrast maps.
    Takes actual and predicted maps with thresholds to apply as input.
    Also returns dice coefficent.

    Parameters
    ----------
    x: numpy array
       contrast map 1
    y: numpy array
       contrast map 2
    x_thr: float
           threshold to apply on x
    y_thr: float
           threshold to apply on y
    Returns
    -------
    list: 6 floats
          True positive rate, False positive rate, accuracy,
          positive predictive value, rombouts overlap, dice coefficient
    """
    x_b = np.zeros_like(x, dtype=bool)
    y_b = np.zeros_like(y, dtype=bool)
    x_b[x > x_thr] = True
    y_b[y > y_thr] = True
    
    if (np.sum(x_b) > 0) and (np.sum(y_b) > 0):
        TP = (np.logical_and(x_b==True, y_b==True)).sum()
        TN = (np.logical_and(x_b==False, y_b==False)).sum()
        FP = (np.logical_and(x_b==True, y_b==False)).sum()
        FN = (np.logical_and(x_b==False, y_b==True)).sum()
        
        P = x_b.sum() # True
        N = (~x_b).sum() # Neg
        
        FP = P-TP
        FN = N-TN
        TPR = TP/float(P)
        TNR = TN/float(N)        
        FPR = FP/float(N)
        FNR = FN/float(P)
        ACC = (TP+TN)/float(P+N)
        PPV = TP/float(TP+TN)
        NPV = TN/float(TN+FN)
        
        R = (2*TP)/float(2*TP+P+N) # Rombouts overlap        
        D = (2*TP)/float(2*TP+FP+FN) # Dice coefficient
        
        return [TPR, FPR, ACC, PPV, R, D]
    else:
        return [np.nan]*6

def get_threshold_scores(subject_ids, yhat, y, contrast_label, contrast_id, model_label, ggm_thresholds=None):
    """Returns DataFrame with hard thresholds and GGM threshold"""
    hard_thresholds = [1.7, 2.3, 3.1]
    df_l = []
    for i,subject_id in enumerate(subject_ids):
        # Get GGM threshold and apply (positive)
        if ggm_thresholds is None:
            models=['noise_activation_and_deactivation']
            _, _, threshold, _ = apply_gamma_fit(y[i], models) # note: threshold is always applied from true subject map
        else:
            threshold = ggm_thresholds[i]
        confused_l = confused(y[i], yhat[i], threshold, threshold)
        l = [subject_id, 'ggm', model_label, contrast_id, contrast_label, threshold] + confused_l
        df_l.append(l)

        # Apply hard choosen thresholds from list
        for threshold in hard_thresholds:
            confused_l = confused(y[i], yhat[i], threshold, threshold)
            l = [subject_id, 'hard', model_label, contrast_id, contrast_label, threshold] + confused_l
            df_l.append(l)
    df = pd.DataFrame(df_l, columns=['Subject', 'Type', 'Model', 'Contrast-ID', 'Contrast', 'Threshold', 'TPR', 'FPR', 'ACC', 'PPV', 'R', 'D'])
    return df
        

def evaluate_predictions(subject_ids, yhat, y, contrast_label, contrast_id, model_label,ggm_thresholds=None):
    # Make dataframe with summary

    dfr = pd.DataFrame(index=subject_ids)
    dfr2 = pd.DataFrame()
    # Pearson R score
    r = np.corrcoef(y, yhat)[0:len(subject_ids),len(subject_ids):]
    dfr['Pearson R'] = np.diag(r)
    dfr['Contrast'] = contrast_label
    dfr['Contrast-ID'] = contrast_id
    dfr['Model'] = model_label
    
    # CV R^2 score
    dfr2['R2-Raw'] = r2_score(y, yhat, multioutput='raw_values')
    dfr2['R2-Variance-Weighted'] = r2_score(y, yhat, multioutput='variance_weighted')
    dfr2['Contrast'] = contrast_label
    dfr2['Contrast-ID'] = contrast_id
    dfr2['Model'] = model_label

    # Threshold scores
    dfthreshold = get_threshold_scores(subject_ids, yhat, y, contrast_label, 
        contrast_id, model_label, ggm_thresholds)
    
    return dfr, dfr2, dfthreshold



def get_contrast_names(contrasts=None):
    """Retrieve selected contrast names, 47"""
    if contrasts == 'full47':
        contrast_indx = [0,1,2,6,7,8,12,13,14,18,19,20,21,22,23,24,25,26,27,28,29,30,44,45,46,\
            50,51,52,56,57,58,59,60,61,62,63,64,65,66,70,71,72,73,74,75,76,77] 
    elif contrasts == None:
        contrast_indx = []
    contrast_ids = {'EMOTION': ['FACES', 'SHAPES', 'FACES-SHAPES',
                                            'neg_FACES', 'neg_SHAPES', 'SHAPES-FACES'],
                                'GAMBLING': ['PUNISH', 'REWARD', 'PUNISH-REWARD',
                                            'neg_PUNISH', 'neg_REWARD', 'REWARD-PUNISH'],
                                'LANGUAGE': ['MATH', 'STORY', 'MATH-STORY',
                                            'STORY-MATH', 'neg_MATH', 'neg_STORY'],
                                'MOTOR': ['CUE', 'LF', 'LH', 'RF', 'RH', 'T',
                                        'AVG', 'CUE-AVG', 'LF-AVG', 'LH-AVG', 'RF-AVG',
                                        'RH-AVG', 'T-AVG', 'neg_CUE', 'neg_LF',
                                        'neg_LH', 'neg_RF', 'neg_RH', 'neg_T',
                                        'neg_AVG', 'AVG-CUE', 'AVG-LF', 'AVG-LH',
                                        'AVG-RF', 'AVG-RH', 'AVG-T'],
                                'RELATIONAL': ['MATCH', 'REL', 'MATCH-REL',
                                                'REL-MATCH', 'neg_MATCH', 'neg_REL'],
                                'SOCIAL': ['RANDOM', 'TOM', 'RANDOM-TOM',
                                            'neg_RANDOM', 'neg_TOM', 'TOM-RANDOM'],
                                'WM': ['2BK_BODY', '2BK_FACE', '2BK_PLACE',
                                        '2BK_TOOL', '0BK_BODY', '0BK_FACE', '0BK_PLACE',
                                        '0BK_TOOL', '2BK', '0BK', '2BK-0BK',
                                        'neg_2BK', 'neg_0BK', '0BK-2BK', 'BODY',
                                        'FACE', 'PLACE', 'TOOL', 'BODY-AVG',
                                        'FACE-AVG', 'PLACE-AVG', 'TOOL-AVG', 'neg_BODY',
                                        'neg_FACE', 'neg_PLACE', 'neg_TOOL', 'AVG-BODY',
                                        'AVG-FACE', 'AVG-PLACE', 'AVG-TOOL']}
    contrast_list = sum(list(contrast_ids.values()), [])
    selected_contrasts = [contrast_list[i] for i in contrast_indx]
    return selected_contrasts


def load_features(subject_ids, feature_path_temp):
    """Loads all features into numpy array (subjects x features x v dimension"""
    X = None
    try: 
        X_sample = np.load(feature_path_temp.format(subject_id=subject_ids[0]))
        if X_sample.ndim > 1:
            n_features = X_sample.shape[0]
            v_dim = X_sample.shape[1]
            X = np.zeros((len(subject_ids), n_features, v_dim))
            for i,subject_id in enumerate(subject_ids):
                X[i,:,:] = np.load(feature_path_temp.format(subject_id=subject_id))
        else:
            X = np.zeros((len(subject_ids), X_sample.shape[0]))
            for i,subject_id in enumerate(subject_ids):
                X[i,:] = np.load(feature_path_temp.format(subject_id=subject_id))
    except Exception as e:
        print(e)
    return X

def load_contrasts(subject_ids, contrast_path_temp):
    Y = None
    try: 
        Y_sample = np.load(contrast_path_temp.format(subject_id=subject_ids[0]))
        v_dim = Y_sample.shape[1]
        Y = np.zeros((len(subject_ids), Y_sample.shape[0], v_dim))
        for i,subject_id in enumerate(subject_ids):
            Y[i,:,:] = np.load(contrast_path_temp.format(subject_id=subject_id))
    except Exception as e:
        print(e)
    return Y