import nibabel as nb
import numpy as np
import matplotlib.pyplot as plt
import os

# TODO
# 1. input validation
# 2. save projection in cache so don't need to recompute everytime.
# 3. compute distortions


def trace_poly(edges):
    '''Given a disjoint set of edges, yield complete linked polygons'''
    idx = dict((i, set([])) for i in np.unique(edges))
    for i, (x, y) in enumerate(edges):
        idx[x].add(i)
        idx[y].add(i)

    eset = set(range(len(edges)))
    while len(eset) > 0:
        eidx = eset.pop()
        poly = list(edges[eidx])
        stack = set([eidx])
        while poly[-1] != poly[0] or len(poly) == 1:
            next = list(idx[poly[-1]] - stack)[0]
            eset.remove(next)
            stack.add(next)
            if edges[next][0] == poly[-1]:
                poly.append(edges[next][1])
            elif edges[next][1] == poly[-1]:
                poly.append(edges[next][0])
            else:
                raise Exception

        yield poly

def boundary_edges(polys):
    '''Returns the edges that are on the boundary of a mesh, as defined by belonging to only 1 face'''
    edges = dict()
    for i, poly in enumerate(np.sort(polys)):
        for a, b in [(0,1), (1,2), (0, 2)]:
            key = poly[a], poly[b]
            if key not in edges:
                edges[key] = []
            edges[key].append(i)

    epts = []
    for edge, faces in edges.items():
        if len(faces) == 1:
            epts.append(edge)

    return np.array(epts)

def make_flat_mask(pts, polys, height=1024):
    """Returns mask and image extent"""
    from PIL import Image, ImageDraw
    bounds = trace_poly(boundary_edges(polys))
    #bounds = bounds.next() # generator object
    bounds = next(bounds)
    aspect = (height / (pts.max(0) - pts.min(0))[1])

    bpts = (pts[bounds] - pts.min(0)) * aspect

    im = Image.new('L', (int(aspect * (pts.max(0) - pts.min(0))[0]), height))
    draw = ImageDraw.Draw(im)
    draw.polygon(bpts[:,:2].ravel().tolist(), fill=255)
    extents = np.hstack([pts.min(0), pts.max(0)])[[0,3,1,4]]
    return np.array(im).T > 0, extents


def make_vertex(pts, polys, mask, extents, height=1024):
    from scipy import sparse
    from scipy.spatial import cKDTree
    valid = np.unique(polys)
    fmax, fmin = pts.max(0), pts.min(0)
    size = fmax - fmin
    aspect = size[0] / size[1]
    width = int(aspect * height)
    grid = np.mgrid[fmin[0]:fmax[0]:width*1j, fmin[1]:fmax[1]:height*1j].reshape(2,-1)

    assert mask.shape[0] == width and mask.shape[1] == height

    kdt = cKDTree(pts[valid,:2])
    dist, vert = kdt.query(grid.T[mask.ravel()])
    dataij = (np.ones((len(vert),)), np.array([np.arange(len(vert)), valid[vert]]))
    return sparse.csr_matrix(dataij, shape=(mask.sum(), len(pts)))

def read_gii(filepath):
    """Reads gifti file and returns pointset vertices: VL x 3 matrix containing
    the x-y-z coordinates of each voxel, # where VL is around 30,000,
    the number of surface voxels in one hemisphere of the brain"""
    img = nb.load(filepath)
    vertices = img.get_arrays_from_intent('pointset')[0].data # vertices (x,y,z)
    triangles = img.get_arrays_from_intent('triangle')[0].data # faces (integers with coordinates in cifti)
    return vertices, triangles

def get_gii_data_vertex_and_poly(img):
    vertices = img.get_arrays_from_intent('pointset')[0].data # vertices (x,y,z)
    triangles = img.get_arrays_from_intent('triangle')[0].data # faces (integers with coordinates in cifti)
    return vertices, triangles

def write_gii(filename, pts, polys):
    pts_darray = gifti.GiftiDataArray.from_array(pts.astype(np.float32), "pointset")
    polys_darray = gifti.GiftiDataArray.from_array(polys, "triangle")
    gii = gifti.GiftiImage(darrays=[pts_darray, polys_darray])
    gifti.write(gii, filename)

def parse_cifti_header_matrix_xml(cifti_img):
    """Returns dictionary with following keys:
         "surface_left_vertex_indices" :
         "surface_left_index_offset_indx" :
         "surface_right_vertex_indices" :
         "surface_right_index_offset_indx :
    """
    """ Broken XML formatter in Nibabel. Need HCP hack below """
    cifti_img_xml = str(cifti_img.header.matrix.to_xml())
    # find IndexOffset, SurfaceNumberOfVertices, VertexIndices. </VertexIndices>
    indx = cifti_img_xml.find('</VertexIndices>')
    surface_left_str = cifti_img_xml[0:indx+16]

    cifti_img_xml = cifti_img_xml[indx+17:]
    indx = cifti_img_xml.find('</VertexIndices>')
    surface_right_str = cifti_img_xml[0:indx+16]


    # Get left and right offset index
    indx = surface_left_str.find('IndexOffset=')
    surface_left_index_offset_indx = int(''.join(s for s in surface_left_str[indx+13:indx+28] if s.isdigit()))
    indx = surface_right_str.find('IndexOffset=')
    surface_right_index_offset_indx = int(''.join(s for s in surface_right_str[indx+13:indx+28] if s.isdigit()))


    surface_left_vertex_indices_str = surface_left_str[\
        surface_left_str.find('<VertexIndices>')+15 : surface_left_str.find('</VertexIndices>')]
    surface_left_vertex_indices = [int(s) for s in surface_left_vertex_indices_str.split() if s.isdigit()]

    surface_right_vertex_indices_str = surface_right_str[\
        surface_right_str.find('<VertexIndices>')+15 : surface_right_str.find('</VertexIndices>')]
    surface_right_vertex_indices = [int(s) for s in surface_right_vertex_indices_str.split() if s.isdigit()]

    cifti_parse_xml_dict = {"surface_left_vertex_indices" : \
                                np.array(surface_left_vertex_indices)+surface_left_index_offset_indx,
                            "surface_left_index_offset_indx" : surface_left_index_offset_indx,
                            "surface_right_vertex_indices" : \
                                np.array(surface_right_vertex_indices)+surface_right_index_offset_indx,
                            "surface_right_index_offset_indx" : surface_right_index_offset_indx}
    return cifti_parse_xml_dict

def get_hcp_flatmap_indices(hcp_cifti_path):
    hcp_cifti_img = nb.load(hcp_cifti_path)
    cifti_parse_xml_dict = parse_cifti_header_matrix_xml(hcp_cifti_img)
    index_left = cifti_parse_xml_dict['surface_left_vertex_indices']
    index_right = cifti_parse_xml_dict['surface_right_vertex_indices']
    offset_left = cifti_parse_xml_dict["surface_left_index_offset_indx"]
    offset_right = cifti_parse_xml_dict["surface_right_index_offset_indx"]

    return index_left, index_right, offset_left, offset_right


def make_flat_img_dict(l_flat_32k_fs_gii, r_flat_32k_fs_gii, hcp_cifti_path=None,
                        cifti_mat=None, index_left=None, index_right=None):
    """Returns dictionary with following keys:
        cortex_left_data" : left_img_data,
        cortex_right_data" : right_img_data,
        cortex_left_vertices" : vertices_left,
        cortex_right_vertices" : vertices_right,
        cortex_left_polys" : polys_left,
        cortex_right_polys": polys_right
        surface_left_vertex_indices" :
        surface_left_index_offset_indx" :
        surface_right_vertex_indices" :
        surface_right_index_offset_indx :
    """
    offset_left = 0
    offset_right = 0

    vertices_left, polys_left = read_gii(l_flat_32k_fs_gii)
    #vertices_left = vertices_left[index_left-offset_left] # select voxels determined from cifti header
    #polys_left = polys_left[index_left-offset_left]
    vertices_right, polys_right = read_gii(r_flat_32k_fs_gii)
    #vertices_right = vertices_right[index_right-offset_right] # select voxels determined from cifti header
    #polys_right = polys_right[index_right-offset_right]

    if hcp_cifti_path is not None:
        print("loading HCP cifti")
        # Get Cifti vertex arrays dictionary with offset added
        hcp_cifti_img = nb.load(hcp_cifti_path)
        cifti_parse_xml_dict = parse_cifti_header_matrix_xml(hcp_cifti_img)
        index_left = cifti_parse_xml_dict['surface_left_vertex_indices']
        index_right = cifti_parse_xml_dict['surface_right_vertex_indices']
        offset_left = cifti_parse_xml_dict["surface_left_index_offset_indx"]
        offset_right = cifti_parse_xml_dict["surface_right_index_offset_indx"]

        hcp_cifti_data = hcp_cifti_img.get_data() # hcp_volume_dim x 91282

        print(hcp_cifti_data.shape)

        left_img_data = np.zeros((hcp_cifti_data.shape[0], 32492))
        left_img_data[:, index_left] = hcp_cifti_data[:, range(0, index_left.shape[0])]

        right_img_data = np.zeros((hcp_cifti_data.shape[0], 32492)) # DataIndices : 29696:59411 29716
        right_img_data[:, index_right-offset_right] = hcp_cifti_data[:, range(offset_right, index_right.shape[0]+offset_right)]

    elif cifti_mat is not None:
        print("using cifti mat")
        offset_left = 0
        offset_right = 29696

        hcp_cifti_data = cifti_mat

        if len(hcp_cifti_data.shape) == 1:
            hcp_cifti_data = np.expand_dims(hcp_cifti_data, axis=0)

        left_img_data = np.zeros((hcp_cifti_data.shape[0], 32492))
        left_img_data[:, index_left] = hcp_cifti_data[:, range(0, index_left.shape[0])]

        right_img_data = np.zeros((hcp_cifti_data.shape[0], 32492)) # DataIndices : 29696:59411 29716
        right_img_data[:, index_right-offset_right] = hcp_cifti_data[:, range(offset_right, index_right.shape[0]+offset_right)]

    else:
        left_img_data = None
        right_img_data = None
        index_left = None
        offset_left = None
        index_right = None
        offset_right = None

    flatmap_data_dict = {"cortex_left_data" : left_img_data,
                         "cortex_right_data" : right_img_data,
                         "cortex_left_vertices" : vertices_left,
                         "cortex_right_vertices" : vertices_right,
                         "cortex_left_polys" : polys_left,
                         "cortex_right_polys": polys_right,
                         "surface_left_vertex_indices" : index_left,
                         "surface_left_index_offset_indx" : offset_left,
                         "surface_right_vertex_indices" : index_right,
                         "surface_right_index_offset_indx" : offset_right,}

    return flatmap_data_dict

def make_hcp_flat_img_dict(lh_img, rh_img, hcp_cifti_img=None, cifti_mat=None,
                            index_left=None, index_right=None):
    """
        cifti_mat should be (n, 32491, 2)
        Returns dictionary with following keys:
        cortex_left_data" : left_img_data,
        cortex_right_data" : right_img_data,
        cortex_left_vertices" : vertices_left,
        cortex_right_vertices" : vertices_right,
        cortex_left_polys" : polys_left,
        cortex_right_polys": polys_right
        surface_left_vertex_indices" :
        surface_left_index_offset_indx" :
        surface_right_vertex_indices" :
        surface_right_index_offset_indx :
    """
    offset_left = 0
    offset_right = 0

    vertices_left = lh_img.get_arrays_from_intent('pointset')[0].data # vertices (x,y,z)
    polys_left = lh_img.get_arrays_from_intent('triangle')[0].data # faces (integers with coordinates in cifti)
    vertices_right = rh_img.get_arrays_from_intent('pointset')[0].data # vertices (x,y,z)
    polys_right = rh_img.get_arrays_from_intent('triangle')[0].data # faces (integers with coordinates in cifti)

    if hcp_cifti_img is not None:
        # Get Cifti vertex arrays dictionary with offset added
        cifti_parse_xml_dict = parse_cifti_header_matrix_xml(hcp_cifti_img)
        index_left = cifti_parse_xml_dict['surface_left_vertex_indices']
        index_right = cifti_parse_xml_dict['surface_right_vertex_indices']
        offset_left = cifti_parse_xml_dict["surface_left_index_offset_indx"]
        offset_right = cifti_parse_xml_dict["surface_right_index_offset_indx"]

        cifti_data = hcp_cifti_img.get_data() # hcp_volume_dim x 91282


        left_img_data = np.zeros((cifti_data.shape[0], 32492))
        left_img_data[:, index_left] = cifti_data[:, range(0, index_left.shape[0])]

        right_img_data = np.zeros((cifti_data.shape[0], 32492)) # DataIndices : 29696:59411 29716
        right_img_data[:, index_right-offset_right] = cifti_data[:, range(offset_right, index_right.shape[0]+offset_right)]
    elif cifti_mat is not None:
        offset_left = 0
        offset_right = 29696

        hcp_cifti_data = cifti_mat

        if len(hcp_cifti_data.shape) == 1:
            hcp_cifti_data = np.expand_dims(hcp_cifti_data, axis=0)

        left_img_data = np.zeros((hcp_cifti_data.shape[0], 32492))
        left_img_data[:, index_left] = hcp_cifti_data[:, range(0, index_left.shape[0])]

        right_img_data = np.zeros((hcp_cifti_data.shape[0], 32492)) # DataIndices : 29696:59411 29716
        right_img_data[:, index_right-offset_right] = hcp_cifti_data[:, range(offset_right, index_right.shape[0]+offset_right)]

    flatmap_data_dict = {"cortex_left_data" : left_img_data,
                         "cortex_right_data" : right_img_data,
                         "cortex_left_vertices" : vertices_left,
                         "cortex_right_vertices" : vertices_right,
                         "cortex_left_polys" : polys_left,
                         "cortex_right_polys": polys_right,
                         "surface_left_vertex_indices" : index_left,
                         "surface_left_index_offset_indx" : offset_left,
                         "surface_right_vertex_indices" : index_right,
                         "surface_right_index_offset_indx" : offset_right,}

    return flatmap_data_dict

def make_hcp_flatmap_img(left_flat_gii, right_flat_gii, hcp_cifti_img=None, \
                            hcp_cifti_mat=None, data_indx=0, height=1024, \
                            index_left=None, index_right=None,):

    """ Take two gifti flatmap hemisphere files along with corresponding cifti
    data file and generate two hemisphere images with data index belonging to
    cifti file and desired image.

    Parameters
    -------------------
    left_flat_gii_path : str
        path to left hemisphere gifti file
    right flat_gii_path : str
        path to right hemisphere gifti file
    cifti_path : str
        path to cifti file
    data_indx : int
        valid index for cifti file to plot

    Other Parameters
    --------------------
    height : int
        height of image (default = 1024)
    index_left : numpy array
        indicies of hcp_cifti_mat on left hemisphere
    index_right : numpy array
        indices of hcp_cifti_mat on right hemisphere

    Output
    --------------------
    img_left, img_right : numpy.array, numpy_array
        two images with selected data projected
    or if data_indx lenght > 1
    img_left_list, img_right_list : [numpy_array], [numpy_array]
    """
    # Parse into dictionary, get mask, extents and pixmap
    if hcp_cifti_img is not None:
        fmd  = make_hcp_flat_img_dict(left_flat_gii, right_flat_gii,\
                                        hcp_cifti_img=hcp_cifti_img)
    elif hcp_cifti_mat is not None:
        fmd = make_hcp_flat_img_dict(left_flat_gii, right_flat_gii,\
                                        cifti_mat=hcp_cifti_mat,\
                                        index_left=index_left,\
                                        index_right=index_right)

    if type(data_indx) is not int:
        img_left_list = []
        assert(fmd['cortex_left_data'].shape[-1] == fmd['cortex_left_vertices'].shape[0])
        mask, extents = make_flat_mask(fmd['cortex_left_vertices'], fmd['cortex_left_polys'], height)
        pixmap = make_vertex(fmd['cortex_left_vertices'], fmd['cortex_left_polys'], mask, extents, height)
        if data_indx is None:
            data_indx = range(0, fmd['cortex_left_data'].shape[0])
        badmask = np.array(pixmap.sum(1) > 0).ravel()
        for i in data_indx:
            data_left = fmd['cortex_left_data'][i]
            img_left = (np.nan*np.ones(mask.shape))
            mimg = (np.nan*np.ones(badmask.shape))
            mimg[badmask] = (pixmap*data_left.ravel())[badmask]
            img_left[mask] = mimg
            img_left_list.append(img_left.T[::-1])
        # Right image
        img_right_list = []
        assert(fmd['cortex_right_data'].shape[-1] == fmd['cortex_right_vertices'].shape[0])
        mask, extents = make_flat_mask(fmd['cortex_right_vertices'], fmd['cortex_right_polys'], height)
        pixmap = make_vertex(fmd['cortex_right_vertices'], fmd['cortex_right_polys'], mask, extents, height)
        badmask = np.array(pixmap.sum(1) > 0).ravel()
        for i in data_indx:
            data_right = fmd['cortex_right_data'][i]
            img_right = (np.nan*np.ones(mask.shape))
            mimg = (np.nan*np.ones(badmask.shape))
            mimg[badmask] = (pixmap*data_right.ravel())[badmask]
            img_right[mask] = mimg
            img_right_list.append(img_right.T[::-1])
        return img_left_list, img_right_list

    else:

        assert(fmd['cortex_left_data'].shape[-1] == fmd['cortex_left_vertices'].shape[0])
        mask, extents = make_flat_mask(fmd['cortex_left_vertices'], fmd['cortex_left_polys'], height)
        pixmap = make_vertex(fmd['cortex_left_vertices'], fmd['cortex_left_polys'], mask, extents, height)
        data_left = fmd['cortex_left_data'][data_indx]
        badmask = np.array(pixmap.sum(1) > 0).ravel()
        img_left = (np.nan*np.ones(mask.shape))
        mimg = (np.nan*np.ones(badmask.shape))
        mimg[badmask] = (pixmap*data_left.ravel())[badmask]
        img_left[mask] = mimg

        assert(fmd['cortex_right_data'].shape[-1] == fmd['cortex_right_vertices'].shape[0])
        mask, extents = make_flat_mask(fmd['cortex_right_vertices'], fmd['cortex_right_polys'], height)
        pixmap = make_vertex(fmd['cortex_right_vertices'], fmd['cortex_right_polys'], mask, extents, height)
        data_right = fmd['cortex_right_data'][data_indx]
        badmask = np.array(pixmap.sum(1) > 0).ravel()
        img_right = (np.nan*np.ones(mask.shape))
        mimg = (np.nan*np.ones(badmask.shape))
        mimg[badmask] = (pixmap*data_right.ravel())[badmask]
        img_right[mask] = mimg

        return img_left.T[::-1], img_right.T[::-1]



def make_flatmap_img(left_flat_gii_path, right_flat_gii_path, cifti_path, data_indx=0, height=1024):

    """ Take two gifti flatmap hemisphere files along with corresponding cifti
    data file and generate two hemisphere images with data index belonging to
    cifti file and desired image.

    Parameters
    -------------------
    left_flat_gii_path : str
        path to left hemisphere gifti file
    right flat_gii_path : str
        path to right hemisphere gifti file
    cifti_path : str
        path to cifti file
    data_indx : int
        valid index for cifti file to plot

    Other Parameters
    --------------------
    height : int
        height of image (default = 1024)

    Output
    --------------------
    img_left, img_right : numpy.array, numpy_array
        two images with selected data projected
    """
    # Parse into dictionary, get mask, extents and pixmap
    if cifti_path is not None:
        fmd  = make_flat_img_dict(left_flat_gii_path, right_flat_gii_path, hcp_cifti_path=cifti_path)
        # Check data is same size as gii
        assert(fmd['cortex_left_data'].shape[-1] == fmd['cortex_left_vertices'].shape[0])
        mask, extents = make_flat_mask(fmd['cortex_left_vertices'], fmd['cortex_left_polys'], height)
        pixmap = make_vertex(fmd['cortex_left_vertices'], fmd['cortex_left_polys'], mask, extents, height)

        data_left = fmd['cortex_left_data'][data_indx]
        badmask = np.array(pixmap.sum(1) > 0).ravel()
        img_left = (np.nan*np.ones(mask.shape))
        mimg = (np.nan*np.ones(badmask.shape))
        mimg[badmask] = (pixmap*data_left.ravel())[badmask]
        img_left[mask] = mimg
        assert(fmd['cortex_right_data'].shape[-1] == fmd['cortex_right_vertices'].shape[0])
        mask, extents = make_flat_mask(fmd['cortex_right_vertices'], fmd['cortex_right_polys'], height)
        pixmap = make_vertex(fmd['cortex_right_vertices'], fmd['cortex_right_polys'], mask, extents, height)
        data_right = fmd['cortex_right_data'][data_indx]
        badmask = np.array(pixmap.sum(1) > 0).ravel()
        img_right = (np.nan*np.ones(mask.shape))
        mimg = (np.nan*np.ones(badmask.shape))
        mimg[badmask] = (pixmap*data_right.ravel())[badmask]
        img_right[mask] = mimg
        return img_left.T[::-1], img_right.T[::-1]
