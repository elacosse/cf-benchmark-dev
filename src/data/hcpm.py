import numpy as np
import pandas as pd
from scipy.io import loadmat
import h5py
import os

from plotting import plot_pred_vs_actual, plot_cortex_cifti, plot_flatmap_reel, plot_flatmap
from cifti_to_flatmap import *

class Subject:
    """Functions for subject specific
        Functions
        ---------
        load_gifti_fm()
            Load the gifti flatmap
        get_flatimg_contrast_dict(contrast_id)
            return flatmap image of contrast with given contrast id


             'get_clean_resting_state',
             'get_contrast_index_and_task_id',
             'get_contrast_map',
             'get_flatimg_contrast_dict',
             'get_flatmap_img_contrast_map',
             'get_flatmap_img_from_cortex_array',
             'get_flatmap_img_rfmri_clean',
             'load_gifti_fm',
             'print_available_hcp'
    """

    # Easy way to retrieve subject data info.
    #

    def __init__(self, subject_id='100307', smoothing_level=2, base_dir=None, cifti_datapath=None):

        if cifti_datapath == None:
            self.cifti_datapath = os.path.join(__file__, 'data_sample')
        else:
            self.cifti_datapath = cifti_datapath

        self.sample_cifti_path = os.path.join(self.cifti_datapath,
                                                    'cifti_sample.nii')  # Initialize HCP index
        # Check if directory of HCP data exists from bash path.
        try:
            if os.path.exists(os.environ["HCPM_BASEDIR"]):
                self.base_dir = os.environ["HCPM_BASEDIR"]
            elif base_dir != None:
                self.base_dir = base_dir
            # self.processed_dir = os.environ['HCPM_PROCESSED_DIR']
        except KeyError:
            print("Error with paths. Did you specify correct path for HCP data?")
        self.subject_id = subject_id
        self.smoothing_level = smoothing_level  # 2mm, 4, 8, 12mm

        # File path templates
        # self.t1w_raw_template = 'structural/raw/{subject_id}/{subject_id}_3T_T1w_MPR1.nii.gz'
        # self.t2w_raw_template = 'structural/raw/{subject_id}/{subject_id}_3T_T2w_SPC1.nii.gz'
        self.fsaverage_LR32k_gii_template = '{subject_id}/MNINonLinear/fsaverage_LR32k/{subject_id}.{hemisphere}.{gii_label}.32k_fs_LR.{gii_type}.gii'
        self.fsaverage_LR32k_nii_template = '{subject_id}/MNINonLinear/fsaverage_LR32k/{subject_id}.{nii_label}.nii'

        self.anatomical_types = ['corrThickness.32k_fs_LR.dscalar',\
                                'curvature.32k_fs_LR.dscalar',\
                                'MyelinMap.32k_fs_LR.dscalar',\
                                'sulc.32k_fs_LR.dscalar',\
                                ] #'ArealDistortion_FS.32k_fs_LR.dscalar',\
        # Sessions 1, 2; enc_dir LR, RL
        self.rfmri_MNI_template = '{subject_id}/MNINonLinear/Results/rfMRI_REST{session_num}_{enc_dir}/rfMRI_REST{session_num}_{enc_dir}.nii.gz'
        self.rfmri_template = '{subject_id}/MNINonLinear/Results/rfMRI_REST{session_num}_{enc_dir}/rfMRI_REST{session_num}_{enc_dir}_Atlas.dtseries.nii' 
        self.rfmri_clean_template = '{subject_id}/MNINonLinear/Results/rfMRI_REST{session_num}_{enc_dir}/rfMRI_REST{session_num}_{enc_dir}_Atlas_hp2000_clean.dtseries.nii'  # ica_fix_denoised_rfmri
        self.tfmri_contrast_template = '{subject_id}/MNINonLinear/Results/tfMRI_{task_id}/tfMRI_{task_id}_hp200_s{smoothing_level}_level2.feat/{subject_id}_tfMRI_{task_id}_level2_hp200_s{smoothing_level}.dscalar.nii'

        self.tfmri_CIFTI_clean_template = '{subject_id}/MNINonLinear/Results/tfMRI_{task_id}_{enc_dir}/tfMRI_{task_id}_{enc_dir}_Atlas.dtseries.nii'
        self.tfmri_MNI_template = '{subject_id}/MNINonLinear/Results/tfMRI_{task_id}_{enc_dir}/tfMRI_{task_id}_{enc_dir}.nii.gz'
        self.tfmri_MNI_design_template = '{subject_id}/MNINonLinear/Results/tfMRI_{task_id}_{enc_dir}/{task_id}_run{run_num}_TAB.txt'
        self.tfmri_ev_template = '{subject_id}/MNINonLinear/Results/tfMRI_{task_id}_{enc_dir}/EVs/{task_sub_id}.txt'


        self.file_available_list = [  # self.t1w_raw_template,
            # self.t2w_raw_template,
            self.fsaverage_LR32k_nii_template,
            self.fsaverage_LR32k_gii_template,
            self.rfmri_MNI_template,
            self.rfmri_clean_template,
            self.tfmri_contrast_template,
            self.tfmri_CIFTI_clean_template,
            self.tfmri_MNI_template,
            self.tfmri_MNI_design_template, ]

        self.contrast_ids = {'EMOTION': ['FACES', 'SHAPES', 'FACES-SHAPES',
                                         'neg_FACES', 'neg_SHAPES', 'SHAPES-FACES'],
                             'GAMBLING': ['PUNISH', 'REWARD', 'PUNISH-REWARD',
                                          'neg_PUNISH', 'neg_REWARD', 'REWARD-PUNISH'],
                             'LANGUAGE': ['MATH', 'STORY', 'MATH-STORY',
                                          'STORY-MATH', 'neg_MATH', 'neg_STORY'],
                             'MOTOR': ['CUE', 'LF', 'LH', 'RF', 'RH', 'T',
                                       'AVG', 'CUE-AVG', 'LF-AVG', 'LH-AVG', 'RF-AVG',
                                       'RH-AVG', 'T-AVG', 'neg_CUE', 'neg_LF',
                                       'neg_LH', 'neg_RF', 'neg_RH', 'neg_T',
                                       'neg_AVG', 'AVG-CUE', 'AVG-LF', 'AVG-LH',
                                       'AVG-RF', 'AVG-RH', 'AVG-T'],
                             'RELATIONAL': ['MATCH', 'REL', 'MATCH-REL',
                                            'REL-MATCH', 'neg_MATCH', 'neg_REL'],
                             'SOCIAL': ['RANDOM', 'TOM', 'RANDOM-TOM',
                                        'neg_RANDOM', 'neg_TOM', 'TOM-RANDOM'],
                             'WM': ['2BK_BODY', '2BK_FACE', '2BK_PLACE',
                                    '2BK_TOOL', '0BK_BODY', '0BK_FACE', '0BK_PLACE',
                                    '0BK_TOOL', '2BK', '0BK', '2BK-0BK',
                                    'neg_2BK', 'neg_0BK', '0BK-2BK', 'BODY',
                                    'FACE', 'PLACE', 'TOOL', 'BODY-AVG',
                                    'FACE-AVG', 'PLACE-AVG', 'TOOL-AVG', 'neg_BODY',
                                    'neg_FACE', 'neg_PLACE', 'neg_TOOL', 'AVG-BODY',
                                    'AVG-FACE', 'AVG-PLACE', 'AVG-TOOL']}


        self.ev_ids = task_designs = {'EMOTION' : ['fear', 'neut'],
                'GAMBLING' : ['loss', 'loss_event', 'neut_event', 'win', 'win_event'],
                'LANGUAGE' : ['cue', 'math', 'present_math', 'present_story', 'question_math', 'question_story',
                              'response_math', 'response_story', 'story'],
                'MOTOR' : ['cue', 'lf', 'lh', 'rf', 'rh', 't'],
                'RELATIONAL' : ['error', 'match', 'relation'],
                'SOCIAL' : ['mental', 'mental_resp', 'other_resp', 'rnd'],
                'WM' : ['0bk_body', '0bk_cor', '0bk_err', '0bk_faces', '0bk_places', '0bk_tools', '2bk_body', '2bk_cor',
                        '2bk_err', '2bk_faces', '2bk_nlr', '2bk_places', '2bk_tools', 'all_bk_cor', 'all_bk_err']}

        self.gii_fm_nb_lh = None
        self.gii_fm_nb_rh = None

        # 32492 cifti cortex per hemisphere => square image would be 180.26^2
        # height = 200 pix for 35192-32492 lh redunancy and 32778-32492
        # height = 193 pix for only 32765-32492 redundant pixels
        self.fm_height = 200
        self.init_for_cifti()

    def init_for_cifti(self):
        self.index_left, self.index_right, self.offset_left, self.offset_right = \
            get_hcp_flatmap_indices(self.sample_cifti_path)

    def print_available_hcp(self):
        print("Files for subject include:")
        print(self.file_available_list)

    def get_all_cifti_anatomy(self):
        """Return all Cifti/Gifti anatomical images as list of Cifti Imgs"""
        imgs = []
        for nii_label in self.anatomical_types:
            filename = self.fsaverage_LR32k_nii_template.format(subject_id=self.subject_id,
                                                                nii_label=nii_label)
            filepath = os.path.join(self.base_dir, filename)
            img = nb.load(filepath)
            imgs.append(img)
        return imgs

    def remap_surface_img(self, cortical_img_data):
        """Hack for XML formatter in Nibabel for handling HCP files"""
        cifti_img = nb.load(self.sample_cifti_path)
        img_data = cifti_img.get_fdata()
        img_data = img_data[0,:]

        cifti_img_xml = str(cifti_img.header.matrix.to_xml())
        # find IndexOffset, SurfaceNumberOfVertices, VertexIndices. </VertexIndices>
        indx = cifti_img_xml.find('</VertexIndices>')
        surface_left_str = cifti_img_xml[0:indx+16]

        cifti_img_xml = cifti_img_xml[indx+17:]
        indx = cifti_img_xml.find('</VertexIndices>')
        surface_right_str = cifti_img_xml[0:indx+16]


        # Get left and right offset index
        indx = surface_left_str.find('IndexOffset=')
        surface_left_index_offset_indx = int(''.join(s for s in surface_left_str[indx+13:indx+28] if s.isdigit()))
        indx = surface_right_str.find('IndexOffset=')
        surface_right_index_offset_indx = int(''.join(s for s in surface_right_str[indx+13:indx+28] if s.isdigit()))


        surface_left_vertex_indices_str = surface_left_str[\
            surface_left_str.find('<VertexIndices>')+15 : surface_left_str.find('</VertexIndices>')]
        surface_left_vertex_indices = [int(s) for s in surface_left_vertex_indices_str.split() if s.isdigit()]

        surface_right_vertex_indices_str = surface_right_str[\
            surface_right_str.find('<VertexIndices>')+15 : surface_right_str.find('</VertexIndices>')]
        surface_right_vertex_indices = [int(s) for s in surface_right_vertex_indices_str.split() if s.isdigit()]

        cifti_parse_xml_dict = {"surface_left_vertex_indices" : \
                                    np.array(surface_left_vertex_indices)+surface_left_index_offset_indx,
                                "surface_left_index_offset_indx" : surface_left_index_offset_indx,
                                "surface_right_vertex_indices" : \
                                    np.array(surface_right_vertex_indices)+surface_right_index_offset_indx,
                                "surface_right_index_offset_indx" : surface_right_index_offset_indx}


        index_left = cifti_parse_xml_dict['surface_left_vertex_indices']
        index_right = cifti_parse_xml_dict['surface_right_vertex_indices']
        offset_left = cifti_parse_xml_dict["surface_left_index_offset_indx"]
        offset_right = cifti_parse_xml_dict["surface_right_index_offset_indx"]
        left_img_data = np.zeros((32492))
        left_img_data[index_left] = img_data[range(0, index_left.shape[0])]
        right_img_data = np.zeros((32492))
        right_img_data[index_right-offset_right] = img_data[range(offset_right, index_right.shape[0]+offset_right)]

        hashmap_left = np.vstack([np.array(range(0, index_left.shape[0])), index_left]) # cifti -> gifti
        hashmap_right = np.vstack([np.array(range(offset_right, index_right.shape[0]+offset_right)), index_right-offset_right])
        
        L = cortical_img_data[0:32492]
        R = cortical_img_data[32492:]
        remapped_cortical_img_data = np.zeros(59412)

        for i, j in zip(hashmap_left[1], hashmap_left[0]):
            remapped_cortical_img_data[hashmap_left[0]] = L[hashmap_left[1]]
        for i, j in zip(hashmap_right[1], hashmap_right[0]):
            remapped_cortical_img_data[hashmap_right[0]] = R[hashmap_right[1]]
        return remapped_cortical_img_data

    def load_gifti_fm(self):

        """Loads the gifti flatmap"""
        # Left
        filename = self.fsaverage_LR32k_gii_template.format(subject_id=self.subject_id,
                                                            hemisphere='L',
                                                            gii_label='flat',
                                                            gii_type='surf')
        fm_path = os.path.join(self.base_dir, filename)
        self.gii_fm_nb_lh = nb.load(fm_path)
        self.file_available_list.append(filename + '___gii_fm_nb_lh')
        # Right
        filename = self.fsaverage_LR32k_gii_template.format(subject_id=self.subject_id,
                                                            hemisphere='R',
                                                            gii_label='flat',
                                                            gii_type='surf')
        fm_path = os.path.join(self.base_dir, filename)
        self.gii_fm_nb_rh = nb.load(fm_path)
        self.file_available_list.append(filename + '___gii_fm_nb_rh')

    def get_task_ev_df(self, task_id, task_sub_id, enc_dir='LR'):
        """Returns DataFrame with design info mat."""
        # Get session num based on encoding direction. Labeled so.
        if enc_dir == 'RL':
            session_num = 1
        elif enc_dir == 'LR':
            session_num = 2

        task_sub_id = task_sub_id.lower()
        filename = self.tfmri_ev_template.format(subject_id=self.subject_id,
                                                 task_id=task_id,
                                                 enc_dir=enc_dir,
                                                 task_sub_id=task_sub_id)
        ev_filepath = os.path.join(self.base_dir, filename)
        df = pd.read_csv(ev_filepath, sep='\t', names=['onset', 'duration', 'value'])
        df['trial_type'] = task_id
        df['trial_subtype'] = task_sub_id

        return df

    def get_flatmap_img_tfmri_clean(self, task_id, enc_dir, index=None):
        if self.gii_fm_nb_lh is None or self.gii_fm_nb_rh is None:
            self.load_gifti_fm()
        # find contrast image
        try:
            img_filename = self.tfmri_CIFTI_clean_template.format(
                subject_id=self.subject_id,
                task_id=task_id,
                enc_dir=enc_dir)
            img_path = os.path.join(self.base_dir, img_filename)
            cifti_img = nb.load(img_path)
            lh_fm_img, rh_fm_img = make_hcp_flatmap_img(self.gii_fm_nb_lh,
                                                        self.gii_fm_nb_rh, hcp_cifti_img=cifti_img, data_indx=index,
                                                        height=self.fm_height)
            return np.stack(lh_fm_img), np.stack(rh_fm_img)  # t, x, y
        except Exception as e:
            print(e)

    def get_flatimg_contrast_dict(self, contrast_id):
        """Returns flatmap img dict"""
        if self.gii_fm_nb_lh is None or self.gii_fm_nb_rh is None:
            self.load_gifti_fm()
        print(contrast_id)
        try:
            for item in self.contrast_ids.items():
                if contrast_id in item[1]:
                    contrast_index = item[1].index(contrast_id)
                    task_id = item[0]
            img_filename = self.tfmri_contrast_template.format(
                subject_id=self.subject_id,
                task_id=task_id,
                smoothing_level=self.smoothing_level)
            img_path = os.path.join(self.base_dir, img_filename)
            cifti_contrast_img = nb.load(img_path)
            fmd = make_hcp_flat_img_dict(self.gii_fm_nb_lh, self.gii_fm_nb_rh,
                                         hcp_cifti_img=cifti_contrast_img)
            return fmd, contrast_index
        except Exception as e:
            print(e)
            print(self.contrast_ids)

    def get_contrast_index_and_task_id(self, contrast_id):
        """return index of contrast file and task_id"""
        try:
            for item in self.contrast_ids.items():
                if contrast_id in item[1]:
                    contrast_index = item[1].index(contrast_id)
                    task_id = item[0]
            return contrast_index, task_id
        except Exception as e:
            print(e)
            print(self.contrast_ids)

    def get_contrast_map(self, contrast_id):
        """Returns contrast dataset of subject under contrast_id"""
        try:
            contrast_index, task_id = self.get_contrast_index_and_task_id(contrast_id)
            img_filename = self.tfmri_contrast_template.format(
                subject_id=self.subject_id,
                task_id=task_id,
                smoothing_level=self.smoothing_level)
            img_path = os.path.join(self.base_dir, img_filename)
            cifti_contrast_img = nb.load(img_path)
            return cifti_contrast_img
        except Exception as e:
            print(e)
            print(self.contrast_ids)

    def get_flatmap_img_contrast_map(self, contrast_id):
        """Returns flatmap image of subject with contrast_id"""
        # check if gifti is already loaded
        if self.gii_fm_nb_lh is None or self.gii_fm_nb_rh is None:
            self.load_gifti_fm()
        # find contrast image
        try:
            contrast_index, task_id = self.get_contrast_index_and_task_id(contrast_id)
            img_filename = self.tfmri_contrast_template.format(
                subject_id=self.subject_id,
                task_id=task_id,
                smoothing_level=self.smoothing_level)
            img_path = os.path.join(self.base_dir, img_filename)
            cifti_contrast_img = nb.load(img_path)
            lh_fm_img, rh_fm_img = make_hcp_flatmap_img(self.gii_fm_nb_lh,
                                                        self.gii_fm_nb_rh, hcp_cifti_img=cifti_contrast_img,
                                                        data_indx=contrast_index, height=self.fm_height)
            return lh_fm_img, rh_fm_img
        except Exception as e:
            print(e)
            print(self.contrast_ids)

    def get_flatmap_img_rfmri_clean(self, session_num, enc_dir, index=None):
        """Returns flatmap image of subject's rfmri. Default returns list of
        entire run.

        Input
        ----------------------
        session_num : 1 or 2
        enc_dir : 'LR' or 'RL'
        index : array, e.g., [0, ..., 1000]"""
        # check if gifti is already loaded
        if self.gii_fm_nb_lh is None or self.gii_fm_nb_rh is None:
            self.load_gifti_fm()
        # find contrast image
        try:
            img_filename = self.rfmri_clean_template.format(
                subject_id=self.subject_id,
                session_num=session_num,
                enc_dir=enc_dir)
            img_path = os.path.join(self.base_dir, img_filename)
            cifti_rfmri_img = nb.load(img_path)
            lh_fm_img, rh_fm_img = make_hcp_flatmap_img(self.gii_fm_nb_lh,
                                                        self.gii_fm_nb_rh, hcp_cifti_img=cifti_rfmri_img,
                                                        data_indx=index,
                                                        height=self.fm_height)
            return np.stack(lh_fm_img), np.stack(rh_fm_img)  # t, x, y
        except Exception as e:
            print(e)

    def get_rsfMRI_MNI(self, session_num=1, enc_dir='LR'):
        """Returns nibabel object with MNI normalized resting-state data."""
        try:
            img_filename = self.rfmri_MNI_template.format(subject_id=self.subject_id,
                                                          session_num=session_num,
                                                          enc_dir=enc_dir)
            img_path = os.path.join(self.base_dir, img_filename)
            img = nb.load(img_path)
            return img
        except Exception as e:
            print(e)

    def get_tfMRI_MNI(self, task_id, enc_dir='LR'):
        """Returns nibabel object with MNI normalized task-based data."""
        try:
            img_filename = self.tfmri_MNI_template.format(subject_id=self.subject_id,
                                                          task_id=task_id,
                                                          enc_dir=enc_dir)
            img_path = os.path.join(self.base_dir, img_filename)
            img = nb.load(img_path)
            return img
        except Exception as e:
            print(e)

    def get_clean_tFMRI_cifti(self, task_id, enc_dir, index=None):
        if self.gii_fm_nb_lh is None or self.gii_fm_nb_rh is None:
            self.load_gifti_fm()
        # find contrast image
        try:
            img_filename = self.tfmri_CIFTI_clean_template.format(
                subject_id=self.subject_id,
                task_id=task_id,
                enc_dir=enc_dir)
            img_path = os.path.join(self.base_dir, img_filename)
            img = nb.load(img_path)
            return img
        except Exception as e:
            print(e)

    def get_resting_state(self, session_num=1, enc_dir='LR'):
        """Returns nibabel object with resting-state data."""
        try:
            img_filename = self.rfmri_template.format(subject_id=self.subject_id,
                                                            session_num=session_num,
                                                            enc_dir=enc_dir)
            img_path = os.path.join(self.base_dir, img_filename)
            img = nb.load(img_path)
            return img
        except Exception as e:
            print(e)
    def get_clean_resting_state(self, session_num=1, enc_dir='LR'):
        """Returns nibabel object with fixed denoised resting-state data."""
        try:
            img_filename = self.rfmri_clean_template.format(subject_id=self.subject_id,
                                                            session_num=session_num,
                                                            enc_dir=enc_dir)
            img_path = os.path.join(self.base_dir, img_filename)
            img = nb.load(img_path)
            return img
        except Exception as e:
            print(e)

    def get_flatmap_img_from_cortex_array(self, cifti_mat):
        """Returns flatmap image from array given, lh, rh"""
        # check if gifti is already loaded
        if self.gii_fm_nb_lh is None or self.gii_fm_nb_rh is None:
            self.load_gifti_fm()
        # Should be in (1, dim) orientation.
        if cifti_mat.shape[0] > 1:
            cifti_mat = cifti_mat
        try:
            # assert(self.gii_fm_nb_lh.shape[0] == cortex_array_lh.shape[0] and \
            # self.gii_fm_nb_rh.shape[0] == cortex_array_rh.shape[0])
            lh_fm_img, rh_fm_img = make_hcp_flatmap_img(self.gii_fm_nb_lh,
                                                        self.gii_fm_nb_rh, hcp_cifti_mat=cifti_mat, data_indx=0,
                                                        height=self.fm_height, index_left=self.index_left, \
                                                        index_right=self.index_right)
            # plt.imshow(rh_fm_img)
            # plt.show()
            return lh_fm_img, rh_fm_img
        except Exception as e:
            print(e)

    def get_region_indicies(self, region_num):
        """Returns voxel indicies that belong to region number of given
        parcellations belonging in Glassner Parcellation. 360 regions."""
        parc = nb.load(os.path.join(self.cifti_datapath,
                                    'Q1-Q6_RelatedParcellation210.CorticalAreas_dil_Colors.32k_fs_LR.dlabel.nii')).get_fdata()
        hcp_atlas_indices = np.zeros(91282, )
        hcp_atlas_indices[0:(29716 + 29696)] = parc
        sub_cort_size = 91282 - (29696 + 29716)
        mask = np.zeros_like(hcp_atlas_indices)
        mask = hcp_atlas_indices == region_num
        return np.where(mask == 1.0), mask

    def get_data_from_parcellation_region(self, data, region_num):
        """Return voxels and their indicies from the Glassner parcellation.
        Assumes time dim last."""
        mask_indx, mask = self.get_region_indicies(region_num)
        return data[mask], mask_indx
