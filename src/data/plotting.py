import matplotlib.pyplot as plt
import seaborn as sns


### Ideas
# Plot flatmap, two hemispheres


def plot_pred_vs_actual(pred_imgs, actual_imgs, save_path=None):
    """Plot 2x2 images of predicted and actual images

    Parameters
    ----------
    pred_imgs : list
    actual_imgs : list
    save_path : str

    Returns
    ----------
    fig

    """

    axlist[0, 0].imshow(lh_img, cmap='RdBu_r')
    axlist[0, 0].pcolormesh(lh_img, vmin=-10., vmax=10., cmap='RdBu_r')
    axlist[0, 1].imshow(rh_img, cmap='RdBu_r')
    axlist[0, 1].pcolormesh(rh_img, vmin=-10., vmax=10., cmap='RdBu_r')
    ############################################################################
    axlist[1, 0].imshow(lh_img, cmap='RdBu_r')
    axlist[1, 0].pcolormesh(lh_img, vmin=-10., vmax=10., cmap='RdBu_r')
    axlist[1, 1].imshow(rh_img, cmap='RdBu_r')
    axlist[1, 1].pcolormesh(rh_img, vmin=-10., vmax=10., cmap='RdBu_r')

def plot_cortex_cifti(hcpdm_subj, cifti_mat, title=None, thresholds=None, figsize=(23, 7)):
    """Return image matrix given data for plotting

        Parameters
    ----------
    hcpdm_subj : HCPDataManager object
    cifti_mat : ndarray
    title : string
    thresholds : list (optional)
        length 2, with [low_bound, upper_bound]
    ax : matplotlib axis, optional
        plots on this axis, if given
    Returns
    -------
    fig : matplotlib figure object
        figure with two axes.
    """
    lh_img, rh_img = hcpdm_subj.get_flatmap_img_from_cortex_array(cifti_mat)
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=figsize, sharey=True)

    if thresholds is not None:
        cm = plt.cm.get_cmap('RdBu')
        lh_img[((lh_img < high_threshold) & (lh_img > low_threshold))] = 0.0
        rh_img[((lh_img < high_threshold) & (rh_img > low_threshold))] = 0.0
    else:
        cm = plt.cm.get_cmap('inferno')
    ax1.imshow(lh_img, cmap=cm);
    ax2.imshow(rh_img, cmap=cm);
    #ax1.axes.get_xaxis().set_visible(False)
    ax1.axes.get_yaxis().set_visible(False)
    # ax2.axes.get_xaxis().set_visible(False)
    ax2.axes.get_yaxis().set_visible(False)
    #fig.tight_layout()
    fig.suptitle(title)
    ax1.set_xlabel('Left Hemisphere')
    ax2.set_xlabel('Right Hemisphere')
    plt.show()


def plot_flatmap_reel(subj_img_dict, subject_id='test'):
    cm = plt.cm.get_cmap('inferno')
    assert(len(subj_img_dict['img_left']) == len(subj_img_dict['img_right']))
    num_plts = len(subj_img_dict['img_left'])

    if num_plts % 5 != 0:   num_plts += 5-(num_plts % 5)
    print(str(num_plts*2) + " are plotting.............")
    f, ax_arr = plt.subplots(num_plts/5, 5, figsize=(100,20))
    ax_arr = [element for tupl in ax_arr for element in tupl]
    f.suptitle(subject_id, fontsize=20)
    for left_plt in zip(ax_arr, range(0, num_plts)):
        try:
            im_left = subj_img_dict['img_left'][left_plt[1]]
            left_plt[0].imshow(im_left, cmap=cm, interpolation='gaussian')
            left_plt[0].axes.get_xaxis().set_visible(False)
            left_plt[0].axes.get_yaxis().set_visible(False)
        except:
            pass
    plt.tight_layout()
    # save plot_flatmaps
    f, ax_arr = plt.subplots(num_plts/5, 5, sharex=True, sharey=True, figsize=(100,20))
    for right_plt in zip(ax_arr, range(0, num_plts)):
        try:
            im_right = subj_img_dict['img_right'][right_plt[1]]
            right_plt[0].imshow(im_right, cmap=cm, interpolation='gaussian')
            right_plt[0].axes.get_xaxis().set_visible(False)
            right_plt[0].axes.get_yaxis().set_visible(False)
        except:
            pass

    """input: numpy array """
    cm = plt.cm.get_cmap('inferno')
    plt.figure()

def plot_flatmap(flatmap_img, ax, title, save_path=None):
    cm = plt.cm.get_cmap('inferno')
    fig = plt.imshow(flatmap_img, cmap=cm, interpolation='gaussian', ax=ax)
    fig.axes.get_xaxis().set_visible(False)
    fig.axes.get_yaxis().set_visible(False)
    if save_path is not None:
        plt.savefig(os.path.join(save_path, str(title)+'.png'), bbox_inches='tight')
        print("saved " + str(os.path.join(save_path, str(subj_id)+'.png')))
    else: plt.show()
