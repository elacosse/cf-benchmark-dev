# -*- coding: utf-8 -*-
import click
import logging
from pathlib import Path
import pandas as pd
from dotenv import find_dotenv, load_dotenv
import os
import sys
import numpy as np

from pca import fast_svd
import sys
import numpy as np
import hcpm


def variance_normalize(X, random_state=1):
    """Variance Normalize input X 
        
        Parameters
        ----------
        X : numpy array (float)
               vertex x time numpy array to process
        Returns
        -------
        Xvn : numpy array
    """
    U,S,V = fast_svd(X, 30, random_state)
    V[np.abs(V) < 2.3*np.std(V)] = 0
    sd = np.std(X - np.dot(np.dot(U, np.diag(S)), V), axis=0)
    sd[np.isnan(sd)] = 0.001
    Xvn = X/sd
    return Xvn 

def get_input_task_data(subj_id, demean=True, normalize=True, random_state=None, cifti_datapath=None):
    """Get preprocessed image data across all task sessions concatenated and variance normalized."""
    img_data_sessions = []
    img_shapes = []
    subject = hcpm.Subject(subject_id=subj_id, cifti_datapath=cifti_datapath)
    tasks = ['EMOTION', 'GAMBLING', 'LANGUAGE', 'MOTOR', 'RELATIONAL', 'SOCIAL', 'WM']
    for enc_dir in ['LR', 'RL']:
        for task in tasks:
            img_data = subject.get_clean_tFMRI_cifti(task, enc_dir).get_fdata()
            img_data = img_data[5:, :] # remove first 5 volumes, t1 stabil habit
            img_data -= img_data.mean(axis=0) # demean
            img_data = variance_normalize(img_data, random_state=random_state) # variance normalize
            img_shapes.append(img_data.shape[0]-5) # NOTICE! EXTRA 5
            img_data_sessions.append(img_data)
    img_data = np.concatenate(img_data_sessions, axis=0)
    img_shapes = np.array(img_shapes)
    return img_data, img_shapes


def get_input_rest_data(subj_id, demean=True, normalize=True, random_state=None, cifti_datapath=None):
    """Get preprocessed image data across all rest sessions concatenated and variance normalized."""
    img_data_sessions = []
    img_shapes = []
    subject = hcpm.Subject(subject_id=subj_id, cifti_datapath=cifti_datapath)
    for enc_dir in ['LR', 'RL']:
        for session in ['1', '2']:
            img_data = subject.get_clean_resting_state(session_num=session, enc_dir=enc_dir).get_fdata()
            img_data = img_data[5:, :] # remove first 5 volumes, t1 stabil habit
            img_data -= img_data.mean(axis=0) # demean
            img_data = variance_normalize(img_data, random_state=random_state) # variance normalize
            img_shapes.append(img_data.shape[0]-5) # NOTICE! EXTRA 5
            img_data_sessions.append(img_data)
    img_data = np.concatenate(img_data_sessions, axis=0)
    img_shapes = np.array(img_shapes)
    return img_data, img_shapes

 

@click.command()
@click.argument('input_filepath', type=click.Path(exists=True))
@click.argument('output_filepath', type=click.Path())
@click.argument('yml_experiment_name', type=click.Path())
def main(input_filepath, output_filepath, yml_experiment_name):
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """

    logger = logging.getLogger(__name__)
    logger.info('running experiment <-- ' + yml_experiment_name)
    logger.info('path to yml: ' + os.path.join(project_dir, 'src', 'experiments'))
    load_dotenv(os.path.join(project_dir, 'src', 'experiments', yml_experiment_name))

    # Do these things:
    PREPROC_RAW = int(os.getenv('PREPROC_RAW')) # process data into concatenated arrays
    GATHER_CONTRASTS = int(os.getenv('GATHER_CONTRASTS')) # concatenate selected contrasts together
    GATHER_ANATOMY = int(os.getenv('GATHER_ANATOMY')) # concatenate selected anatomical features together

    RANDOM_SEED = int(os.getenv('RANDOM_SEED'))
    SUBJECT_CSV_NAME =  os.getenv("SUBJECT_CSV_NAME")
    GLM_SMOOTH = os.getenv("GLM_SMOOTH") # 4mm smoothing kernel applied for contrast targets (precomputed)
    CONTRAST_IDS = [int(c) for c in os.getenv('CONTRAST_INDICES')[1:-1].split(',')]
    logger.info('making final data set from raw data')

    # If HCPM_BASEDIR where HCP data is located isn't set already.
    # THis must be a directory that has the normal HCP data folder structure.
    if os.environ["HCPM_BASEDIR"] == None:
        os.environ["HCPM_BASEDIR"] = input_filepath
    else:
        HCPM_BASEDIR = os.getenv('HCPM_BASEDIR')

    logger.info('Random state for process: ' + str(RANDOM_SEED))
    
    # get subject ids
    subjects_df = pd.read_csv(os.path.join(project_dir, 'data/external/subject_lists/', SUBJECT_CSV_NAME))
    subjects = subjects_df['Subject']

    # run preprocessing transformations on raw
    if PREPROC_RAW:
        
        if not os.path.exists(os.path.join(output_filepath, 'rsfmri')):
            os.makedirs(os.path.join(output_filepath, 'rsfmri'))
        if not os.path.exists(os.path.join(output_filepath, 'tfmri')):
            os.makedirs(os.path.join(output_filepath, 'tfmri'))
        for subj_id in subjects:

            # rsfmri data
            try:
                if not os.path.exists(os.path.join(output_filepath, 'rsfmri', str(subj_id)+'_rest_data.npy')):
                    logger.info('rsfMRI preproc  ' + str(subj_id) + ' ...')
                    img_data, img_shapes = get_input_rest_data(subj_id, random_state=RANDOM_SEED, 
                        cifti_datapath=os.path.join(project_dir, 'data/external/data_sample'))
                    np.save(os.path.join(output_filepath, 'rsfmri', str(subj_id)+'_rest_data'), img_data)
                else:
                    logger.info('rsfMRI ' + str(subj_id) + '  already exists ...')
            except Exception as e:
                logger.error(e)

            # Task Data 
            try:
                if not os.path.exists(os.path.join(output_filepath, 'tfmri', str(subj_id)+'_task_data.npy')):
                    logger.info('tfMRI preproc  ' + str(subj_id) + ' ...')
                    img_data, img_shapes = get_input_task_data(subj_id, random_state=RANDOM_SEED,
                        cifti_datapath=os.path.join(project_dir, 'data/external/data_sample'))
                    np.save(os.path.join(output_filepath, 'tfmri', str(subj_id)+'_task_data'), img_data)
                    np.save(os.path.join(output_filepath, 'tfmri', str(subj_id)+'_task_data_shapes'), img_shapes)
                else:
                    logger.info('tfMRI ' + str(subj_id) + ' already exists ...')
            except Exception as e:
                logger.error(e)
            logger.info('-> processed ' + str(subj_id)+ ' rsfmri and tfmri')

    # gather all contrasts targets into arrays
    if GATHER_CONTRASTS:
        contrast_list = ['FACES', 'PUNISH', 'MATH', 'CUE', 'MATCH', 'RANDOM', '2BK_BODY']
        contrast_ind = CONTRAST_IDS
        if not os.path.exists(os.path.join(output_filepath, 'contrasts')):
            os.makedirs(os.path.join(output_filepath, 'contrasts'))
        for subj_id in subjects:
            try:
                filepath = os.path.join(output_filepath, 'contrasts', str(subj_id)+'.npy')
                if not os.path.isfile(filepath):
                    contrast = []
                    for c_id in contrast_list:
                        subject = hcpm.Subject(subject_id=subj_id, smoothing_level=GLM_SMOOTH, cifti_datapath=os.path.join(project_dir, 'data/external/data_sample'))
                        c = subject.get_contrast_map(c_id).get_fdata()
                        contrast.append(c)
                    contrast = np.concatenate(contrast, axis=0)
                    contrast = contrast[list(contrast_ind),:]
                    np.save(filepath, contrast)
                    logger.info('-> processed ' + str(subj_id) + ' contrasts')
                else:
                    logger.info('contrast target file for ' + str(subj_id) + ' already exists ...')
            except Exception as e:
                logger.error(e)

    # Anatomical data
    if GATHER_ANATOMY:
        if not os.path.exists(os.path.join(output_filepath, 'anatomy')):
            os.makedirs(os.path.join(output_filepath, 'anatomy'))
        for subj_id in subjects:
            subject = hcpm.Subject(subject_id=subj_id, smoothing_level=GLM_SMOOTH, cifti_datapath=os.path.join(project_dir, 'data/external/data_sample'))
            # get anatomy
            anat = []
            anat_imgs = subject.get_all_cifti_anatomy()
            for img in anat_imgs:
                if img.shape[1] > 59412:
                    # remap onto correct surface
                    data = subject.remap_surface_img(img.get_fdata()[0])
                    anat.append(data)
                else:
                    anat.append(img.get_fdata()[0])
            img_data = subject.get_clean_resting_state(session_num=1, enc_dir='LR').get_fdata()
            meanepi_lr = img_data.mean(axis=0)
            meanepi_lr -= meanepi_lr.mean()
            meanepi_lr /= meanepi_lr.std()
            anat.append(meanepi_lr[0:59412]) # take only surface
            img_data = subject.get_clean_resting_state(session_num=1, enc_dir='RL').get_fdata()
            meanepi_rl = img_data.mean(axis=0)
            meanepi_rl -= meanepi_rl.mean()
            meanepi_rl /= meanepi_rl.std()
            anat.append(meanepi_rl[0:59412]) # take only surface
            anat = np.vstack(anat)

            filepath = os.path.join(output_filepath, 'anatomy', str(subj_id)+'.npy')
            np.save(filepath, anat)
            logger.info('-> processed ' + str(subj_id) + ' anatomy')
            
if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv(), verbose=True)
    main()

