#!/bin/bash

# Use aws utility to download data from HCP project servers

subjectlist=data/external/200_Subject_100F_100M_r227_fullTask_fullRest_Unrelated_S900_Subject_multilist1_with_physio.txt
dir_target=data/raw


tasks="
rfMRI_REST1_LR
rfMRI_REST1_RL
rfMRI_REST2_LR
rfMRI_REST2_RL
tfMRI_EMOTION_LR
tfMRI_EMOTION_RL
tfMRI_GAMBLING_LR
tfMRI_GAMBLING_RL
tfMRI_LANGUAGE_LR
tfMRI_LANGUAGE_RL
tfMRI_MOTOR_LR
tfMRI_MOTOR_RL
tfMRI_RELATIONAL_LR
tfMRI_RELATIONAL_RL
tfMRI_SOCIAL_LR
tfMRI_SOCIAL_RL
tfMRI_WM_LR
tfMRI_WM_RL
"

while read -r subject;
do
    echo $subject
    # Recursively copy all task-based maps from level 2 analysis
    for filedir_id in $tasks;
    do
      dir_hcp=$subject/MNINonLinear/Results/$filedir_id
      echo $dir_hcp
      aws s3 cp \
        s3://hcp-openaccess/HCP_1200/$dir_hcp/ \
        $dir_target/$dir_hcp/ \
        --recursive --region eu-west-1 \
        --exclude "*" \
        --include "*.dtseries.nii" \
        --include "*.txt"
    done;

done < $subjectlist


tasks="
tfMRI_EMOTION
tfMRI_GAMBLING
tfMRI_LANGUAGE
tfMRI_MOTOR
tfMRI_RELATIONAL
tfMRI_SOCIAL
tfMRI_WM
"

while read -r subject;
do
    echo $subject
    # Recursively copy all FEAT task-based maps from level 2 analysis
    for filedir_id in $tasks;
    do
      dir_hcp=$subject/MNINonLinear/Results/$filedir_id/${filedir_id}_hp200_s4_level2.feat
      echo $dir_hcp
      if [ ! -d dir_hcp ]; then
	      aws s3 cp \
		s3://hcp-openaccess/HCP_1200/$dir_hcp \
		$dir_target/$dir_hcp/ \
		--recursive --region eu-west-1 \
      fi
    done;
done < $subjectlist