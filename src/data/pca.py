from __future__ import division
from math import ceil
import itertools
import glob
from distutils.version import LooseVersion

import numpy as np
from scipy import linalg
from sklearn.utils import check_random_state
from sklearn.utils.extmath import randomized_svd, svd_flip

"""Apply fast_svd from nilearn module: https://nilearn.github.io/"""

def fast_svd(X, n_components, random_state=None):
    """ Automatically switch between randomized and lapack SVD (heuristic
        of scikit-learn).
    Parameters
    ==========
    X: array, shape (n_samples, n_features)
        The data to decompose
    n_components: integer
        The order of the dimensionality of the truncated SVD
    random_state: int or RandomState
        Pseudo number generator state used for random sampling.
    Returns
    ========
    U: array, shape (n_samples, n_components)
        The first matrix of the truncated svd
    S: array, shape (n_components)
        The second matric of the truncated svd
    V: array, shape (n_components, n_features)
        The last matric of the truncated svd
    """
    random_state = check_random_state(random_state)
    # Small problem, just call full PCA
    if max(X.shape) <= 500:
        svd_solver = 'full'
    elif n_components >= 1 and n_components < .8 * min(X.shape):
        svd_solver = 'randomized'
    # This is also the case of n_components in (0,1)
    else:
        svd_solver = 'full'

    # Call different fits for either full or truncated SVD
    if svd_solver == 'full':
        U, S, V = linalg.svd(X, full_matrices=False)
        # flip eigenvectors' sign to enforce deterministic output
        U, V = svd_flip(U, V)
        # The "copy" are there to free the reference on the non reduced
        # data, and hence clear memory early
        U = U[:, :n_components].copy()
        S = S[:n_components]
        V = V[:n_components].copy()
    else:
        n_iter = 'auto'

        U, S, V = randomized_svd(X, n_components=n_components,
                                 n_iter=n_iter,
                                 flip_sign=True,
                                 random_state=random_state)
    return U, S, V



def reduce_single(this_data, confound,
                            reduction_ratio=None,
                            n_samples=None,
                            memory=None,
                            memory_level=0,
                            random_state=None):
    """Utility function for multiprocessing from MaskReducer"""
    # Now get rid of the img as fast as possible, to free a
    # reference count on it, and possibly free the corresponding
    # data
    random_state = check_random_state(random_state)

    data_n_samples = this_data.shape[0]
    if reduction_ratio is None:
        assert n_samples is not None
        n_samples = min(n_samples, data_n_samples)
    else:
        n_samples = int(ceil(data_n_samples * reduction_ratio))

    # U, S, V = cache(fast_svd, memory,
    #                     memory_level=memory_level,
    #                     func_memory_level=3)(this_data.T,
    #                                          n_samples,
    #                                          random_state=random_state)

    U,S,V = fast_svd(this_data.T, n_samples, random_state=random_state)
    U = U.T.copy()
    U = U * S[:, np.newaxis]
    return U



