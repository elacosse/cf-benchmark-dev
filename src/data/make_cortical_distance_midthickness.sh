#!/bin/bash

# Calculates distance of vertices from cortical surface average

inpath=$1
outpath=$2

hemisphere='R'

for vertex_indx in {0..32492}
do
  outfile=$outpath/vertex_R_$vertex_indx.shape.gii
  echo $vertex_indx, $outfile
  wb_command -surface-geodesic-distance $inpath/100TRSubjects.R.average.midthickness.32k_fs_LR.surf.gii $vertex_indx $outfile
done
echo All done

hemisphere='L'

for vertex_indx in {0..32492}
do
  outfile=$outpath/vertex_L_$vertex_indx.shape.gii
  echo $vertex_indx, $outfile
  wb_command -surface-geodesic-distance $inpath/100TRSubjects.L.average.midthickness.32k_fs_LR.surf.gii $vertex_indx $outfile
done
echo All done

