# -*- coding: utf-8 -*-
"""
    Creates distance information that can be used when masking vertices/voxels is needed for certain models.
    Takes output from get_cortical_distance_midthickness.sh relying on connectome workbench functions. 
    This needs to run before these routines.
"""


import click
import logging
from pathlib import Path
import pandas as pd
from dotenv import find_dotenv, load_dotenv
import os, sys
import numpy as np
import nibabel as nb
import hcpm

def parse_cifti_header_matrix_xml(cifti_img):
    """Returns dictionary with following keys:
         "surface_left_vertex_indices" :
         "surface_left_index_offset_indx" :
         "surface_right_vertex_indices" :
         "surface_right_index_offset_indx :
    """
    """ Broken? XML formatter in Nibabel. Need HCP hack below """
    cifti_img_xml = str(cifti_img.header.matrix.to_xml())
    # find IndexOffset, SurfaceNumberOfVertices, VertexIndices. </VertexIndices>
    indx = cifti_img_xml.find('</VertexIndices>')
    surface_left_str = cifti_img_xml[0:indx+16]

    cifti_img_xml = cifti_img_xml[indx+17:]
    indx = cifti_img_xml.find('</VertexIndices>')
    surface_right_str = cifti_img_xml[0:indx+16]


    # Get left and right offset index
    indx = surface_left_str.find('IndexOffset=')
    surface_left_index_offset_indx = int(''.join(s for s in surface_left_str[indx+13:indx+28] if s.isdigit()))
    indx = surface_right_str.find('IndexOffset=')
    surface_right_index_offset_indx = int(''.join(s for s in surface_right_str[indx+13:indx+28] if s.isdigit()))


    surface_left_vertex_indices_str = surface_left_str[\
        surface_left_str.find('<VertexIndices>')+15 : surface_left_str.find('</VertexIndices>')]
    surface_left_vertex_indices = [int(s) for s in surface_left_vertex_indices_str.split() if s.isdigit()]

    surface_right_vertex_indices_str = surface_right_str[\
        surface_right_str.find('<VertexIndices>')+15 : surface_right_str.find('</VertexIndices>')]
    surface_right_vertex_indices = [int(s) for s in surface_right_vertex_indices_str.split() if s.isdigit()]

    cifti_parse_xml_dict = {"surface_left_vertex_indices" : \
                                np.array(surface_left_vertex_indices)+surface_left_index_offset_indx,
                            "surface_left_index_offset_indx" : surface_left_index_offset_indx,
                            "surface_right_vertex_indices" : \
                                np.array(surface_right_vertex_indices)+surface_right_index_offset_indx,
                            "surface_right_index_offset_indx" : surface_right_index_offset_indx}
    return cifti_parse_xml_dict



@click.command()
@click.argument('input_filepath', type=click.Path(exists=True))
@click.argument('output_filepath', type=click.Path())
def main(input_filepath, output_filepath=os.path.join(project_dir, 'data', 'interim', 'distance')):
    """
        Parameters
        Saves to output_filepath -- default is data/interim/distance

    """
    ######### Sub-cortical #####################
    # serial looooonng computation time
    img = nb.load(os.path.join(project_dir, 'data', 'external', 'Atlas_ROIs.2.nii.gz'))
    img_data = img.get_fdata()
    voxel_indx = np.where(img_data > 0)
    voxel_size = 2 #mm
    dist = np.zeros((31870, 31870))
    from scipy.spatial import distance
    for x1,y1,z1,t1 in zip(voxel_indx[0], voxel_indx[1], voxel_indx[2], range(0, 31870)):
        for x2,y2,z2,t2 in zip(voxel_indx[0], voxel_indx[1], voxel_indx[2], range(0, 31870)):
            dist[t1,t2] = distance.euclidean((x1,y1,z1), (x2,y2,z2)) * voxel_size

    np.save(os.path.join(output_filepath, 'subcortical_euclidean_2mm.npy'), dist)
    
    ######### Cortical Surface #################
    # gifti distance temp
    #/mnt/50tbd/HCP_derivatives/cortical_distances
    img_temp = os.path.join(input_filepath, 'vertex_{hemi}_{vertex}.shape.gii')
    #img_temp  = os.path.join(project_dir, 'data', 'interim', 'vertex_{hemi}_{vertex}.shape.gii')
    dist_L = np.zeros((32492, 32492), dtype='float32')
    for i in range(0, 32492):
        img = nb.load(img_temp.format(vertex=i, hemi='L'))
        dist_L[i] = img.darrays[0].data
    dist_R = np.zeros((32492, 32492), dtype='float32')
    for i in range(0, 32492):
        img = nb.load(img_temp.format(vertex=i, hemi='R'))
        dist_R[i] = img.darrays[0].data

    # construct hashmap from example cifti
    subject = hcpm.Subject(cifti_datapath=os.path.join(project_dir, 'data/external/data_sample'))
    img = nb.load(subject.sample_cifti_path)
    img_data = img.get_fdata()
    img_data = img_data[0,:]

    cifti_parse_xml_dict = parse_cifti_header_matrix_xml(img)
    index_left = cifti_parse_xml_dict['surface_left_vertex_indices']
    index_right = cifti_parse_xml_dict['surface_right_vertex_indices']
    offset_left = cifti_parse_xml_dict["surface_left_index_offset_indx"]
    offset_right = cifti_parse_xml_dict["surface_right_index_offset_indx"]
    left_img_data = np.zeros((32492))
    left_img_data[index_left] = img_data[range(0, index_left.shape[0])]
    right_img_data = np.zeros((32492))
    right_img_data[index_right-offset_right] = img_data[range(offset_right, index_right.shape[0]+offset_right)]


    hashmap_left = np.vstack([np.array(range(0, index_left.shape[0])), index_left]) # cifti -> gifti
    hashmap_right = np.vstack([np.array(range(offset_right, index_right.shape[0]+offset_right)), index_right-offset_right])

    x = np.zeros((59412, 59412), dtype='float32')
    for i, j in zip(hashmap_left[1], hashmap_left[0]):
        x[j,hashmap_left[0]] = dist_L[i][hashmap_left[1]]
    for i, j in zip(hashmap_right[1], hashmap_right[0]):
        x[j,hashmap_right[0]] = dist_R[i][hashmap_right[1]]

    np.save(os.path.join(output_filepath, 'distance_cortical_surface.npy'), x)


if __name__ == '__main__':
    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())
    
    main()






