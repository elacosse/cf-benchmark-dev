******************************************
cf-benchmark-dev : a ''connectome-fingerprinting'' investigation for task-fMRI prediction from resting-state fMRI data.

******************************************

# Introduction
============

* Author: Eric Lacosse
* Web site: http://gitlab.com/elacosse/cf-benchmark-dev
* Version: 0.0.2
* License: The MIT License (MIT) LICENSE

Subject IDs are found in data/external.


# Project Organization
------------

    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources. This includes subject lists,
    |   |                     subcortical atlas ROIs, and a combined MMP-1.0 and subcortical
    |   |                     parcellation used in many feature extraction routines.
    │   ├── interim        <- Intermediate data.
    │   ├── processed      <- The final, canonical data sets for modeling and output directories
    │   └── raw            <- The original, immutable data dump.
    │
    │
    ├── models             <- Trained and serialized models
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                          the creator's initials, and a short `-` delimited description,
    |                          e.g. `1.0-jqp-initial-data-exploration`.
    │
    ├── reports            <- Generated analysis for reporting.
    │
    ├── environment.yml    <- The conda .yml file for reproducing the analysis environment.
    |                         To create environment `cf-benchmark-dev`, do ```make requirements```.
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │    
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   ├── make_dataset.py
    |   |   ├── make_go_distance_information.py
    |   |   |              <- Calculate surface distances.
    │   │   └── hcpm.py    <- HCP specific loading class for handling that specific data. Additional plotting tools for flatmap visualization. 
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   └── build_features.py
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │   │   │                 predictions
    │   │   ├── test_model.py
    │   │   └── train_model.py
    │   │
    │   └── Evaluation     <- Scripts to create summaries for evaluating the model outputs


--------

Development status
==================
This is research code. Please report any issues or bugs encountered.

Use Overview
==================
Preliminaries, preprocessing, fitting, prediction, and evaluation routines can be handled through use of the included ``Makefile`` commands. 

Feature extraction, model fitting, and evaluation options are set by ```experiments/100_train_100_test.yml```.
This file includes different flags for use in any make calls.

## Getting data 
If data has not been downloaded already, all HCP data can be acquired from the HCP project via an AWS utility like in the bash script located in 
```
src/data/download_hcp_project_set.sh
```

## Calculate distance information for cortical surface and subcortical volumetric component.
```
make distance
```
Distance information is often incorporated for some modeling routines. 
In these cases, a midthickness surface averaged from all 100 training subjects is derived and used to calculate geodesic distances. 
Due to its small file size, it is included in ``data/external/surface_template/100TRSubjects.{hemisphere_L_or_R}.average.midthickness.32k_fs_LR.surf.gii``. 
Its derivation was performed by the HCP connectome workbench tool--``wb_command -surface-average``
Calculation of surface distances is performed using the HCP connectome workbench tool as well.
Finally, volumetric distances are calculated and combined together with the previous calculation in ``src/data/make_go_distance_information.py``.
## Preprocess functional data and contrasts
```
make data
```
All functional data is processed and all contrasts GLM maps at appropriate smoothing level (surface 4mm) are gathered to be saved in ```data/processed```.
## Extract features
```
make features
```
## Model fitting
```
make train_models
```
## Model prediction
```
make test_models
```
## Model evaluation
```
make evaluate_models
```
## Other (Matlab req.)
Group GLM analyses, e.g., TFCE are performed on the surface using the matlab tool ``PALM``
https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/PALM.

@article{winkler2014permutation,
  title={Permutation inference for the general linear model},
  author={Winkler, Anderson M and Ridgway, Gerard R and Webster, Matthew A and Smith, Stephen M and Nichols, Thomas E},
  journal={Neuroimage},
  volume={92},
  pages={381--397},
  year={2014},
  publisher={Elsevier}
}


Acknowledgements
================

Many model fits are performed using library funcitons from scikit-learn

@article{pedregosa2011scikit,
  title={Scikit-learn: Machine learning in Python},
  author={Pedregosa, Fabian and Varoquaux, Ga{\"e}l and Gramfort, Alexandre and Michel, Vincent and Thirion, Bertrand and Grisel, Olivier and Blondel, Mathieu and Prettenhofer, Peter and Weiss, Ron and Dubourg, Vincent and others},
  journal={the Journal of machine Learning research},
  volume={12},
  pages={2825--2830},
  year={2011},
  publisher={JMLR. org}
}

The hcpm module uses code from the following project for flatmap visualization:

@article{gao2015pycortex,
  title={Pycortex: an interactive surface visualizer for fMRI},
  author={Gao, James S and Huth, Alexander G and Lescroart, Mark D and Gallant, Jack L},
  journal={Frontiers in neuroinformatics},
  volume={9},
  pages={23},
  year={2015},
  publisher={Frontiers}
}
